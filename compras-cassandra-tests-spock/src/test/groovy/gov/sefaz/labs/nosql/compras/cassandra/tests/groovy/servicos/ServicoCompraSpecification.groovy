package gov.sefaz.labs.nosql.compras.cassandra.tests.groovy.servicos

import gov.sefaz.labs.nosql.compras.cassandra.Application
import gov.sefaz.labs.nosql.compras.cassandra.tests.utils.TestUtils
import gov.sefaz.labs.nosql.compras.core.dominio.Compra
import gov.sefaz.labs.nosql.compras.core.persistencia.RepositorioCompra
import gov.sefaz.labs.nosql.compras.core.servicos.ServicoCompra
import gov.sefaz.labs.nosql.compras.core.utils.GeradorDeDados
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

import java.text.SimpleDateFormat

@ContextConfiguration(classes = Application, loader = SpringApplicationContextLoader)
class ServicoCompraSpecification extends Specification {

    @Autowired
    ServicoCompra servicoCompra;

    @Autowired
    RepositorioCompra repositorioCompra;

    void setup() {
        repositorioCompra.excluaTodos();
    }

    void cleanup() {
        repositorioCompra.excluaTodos();
    }

    void "insercao de um registro"() {
        setup:
        servicoCompra.insira(compra)

        when: "os dados de uma compra foram enviados ao serviço de insercao"
        def compras = repositorioCompra.liste()

        then: "a quantidade de compras na base deve ser igual a 1"
        compras.size() == 1

        where: "os dados da compra são aleatórios e a lista de compras é consultada diretamente do repositório"
        geracaoDeDados = new GeradorDeDados()
        compra = geracaoDeDados.obtenhaCompraComDadosAleatorios()
    }

    void "consulta a um registro a partir do ID"() {
        setup:
        def id = servicoCompra.insira(compra)

        when:
        def resultadoConsulta = servicoCompra.consulte(id)

        then:
        resultadoConsulta != null

        where:
        compra = TestUtils.obtenhaInstanciaCompra()
    }

    void "consulta à lista de compras a partir de uma data"() {
        setup:
        compra1.setDataHora(data1)
        compra2.setDataHora(data1)
        compra3.setDataHora(data2)
        servicoCompra.insira(compra1)
        servicoCompra.insira(compra2)
        servicoCompra.insira(compra3)

        when:
        def resultadoConsulta = servicoCompra.listePorData(data1)

        then:
        resultadoConsulta.size() == 2

        where:
        dateFormat = new SimpleDateFormat("dd/MM/yyyy")
        data1 = dateFormat.parse("15/05/2014")
        data2 = dateFormat.parse("20/11/2013")
        compra1 = TestUtils.obtenhaInstanciaCompra()
        compra2 = TestUtils.obtenhaInstanciaCompra()
        compra3 = TestUtils.obtenhaInstanciaCompra()
    }

    void "consulta à lista completa de registros"() {
        setup:
        servicoCompra.insira(compra1)
        servicoCompra.insira(compra2)

        when:
        def resultadoConsulta = servicoCompra.liste()

        then:
        resultadoConsulta.size() == 2

        where:
        compra1 = TestUtils.obtenhaInstanciaCompra()
        compra2 = TestUtils.obtenhaInstanciaCompra()
    }

    void "alteração de um registro"() {
        setup:
        def id = servicoCompra.insira(compra)

        when:
        compra.setComentario(novoComentario)
        servicoCompra.atualize(compra)
        def resultadoConsulta = servicoCompra.consulte(id)

        then:
        resultadoConsulta.getComentario().equals(novoComentario)

        where:
        compra = TestUtils.obtenhaInstanciaCompra()
        novoComentario = "novo comentário"
    }

    void "exclusão de um registro"() {
        setup:
        def id = servicoCompra.insira(compra)

        when:
        def instanciaExclusao = new Compra()
        instanciaExclusao.setId(id)
        servicoCompra.exclua(instanciaExclusao)

        then:
        servicoCompra.liste().size() == 0

        where:
        compra = TestUtils.obtenhaInstanciaCompra()
    }
}
