package gov.sefaz.labs.nosql.compras.cassandra.tests.groovy.web

import gov.sefaz.labs.nosql.compras.cassandra.Application
import org.springframework.boot.SpringApplication
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestTemplate
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification

import java.util.concurrent.Callable
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit

class ControllerInserirRegistrosSpecification extends Specification {

    @Shared
    @AutoCleanup
    ConfigurableApplicationContext context;

    void setupSpec() {
        Future<ConfigurableApplicationContext> future = Executors.newSingleThreadExecutor().submit(
            new Callable() {
                @Override
                public ConfigurableApplicationContext call() throws Exception {
                    return (ConfigurableApplicationContext) SpringApplication.run(Application.class);
                }
            });

        context = future.get(60, TimeUnit.SECONDS);
    }

    void "testa mensagem de sucesso do metodo de insercao"() {
        when: "A requisição for disparada ao controller"
        ResponseEntity<String> resposta = new RestTemplate().getForEntity(url, String.class);

        then: "Uma mensagem de sucesso deve ser retornada"
        resposta.statusCode == HttpStatus.OK
        resposta.body.contains("Registro = ")

        where:
        metodo = "insira"
        parametros = "registros=10000&step=5000&valoresFixos=false"
        url = "http://localhost:8080/" + metodo + "?" + parametros
    }

}
