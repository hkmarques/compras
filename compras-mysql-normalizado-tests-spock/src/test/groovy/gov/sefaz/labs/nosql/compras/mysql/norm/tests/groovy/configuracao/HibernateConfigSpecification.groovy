package gov.sefaz.labs.nosql.compras.mysql.norm.tests.groovy.configuracao

import gov.sefaz.labs.nosql.compras.mysql.norm.Application
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

import javax.annotation.Resource

@ContextConfiguration(classes = Application, loader = SpringApplicationContextLoader)
class HibernateConfigSpecification extends Specification {

    @Resource(name = "hibernateProperties")
    Properties hibernateProperties;

    @Resource(name = "dataSourceProperties")
    Properties dataSourceProperties;

    void "carregamento do arquivo de propriedades do Hibernate"() {
        expect: "Um valor qualquer definido no arquivo de configurações"
        hibernateProperties.getProperty("test.property") == "valor de teste em hibernate.properties"
    }

    void "carregamento do arquivo de propriedades do data source"() {
        expect: "Um valor definido no arquivo de configurações (propriedades de nomes arbitrários resultarão em erro em tempo de execução)"
        dataSourceProperties.getProperty("user") == "compras"
    }

}
