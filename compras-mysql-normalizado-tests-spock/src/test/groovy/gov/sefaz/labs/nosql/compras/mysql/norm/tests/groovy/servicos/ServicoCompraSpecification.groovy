package gov.sefaz.labs.nosql.compras.mysql.norm.tests.groovy.servicos

import gov.sefaz.labs.nosql.compras.core.dominio.Compra
import gov.sefaz.labs.nosql.compras.core.persistencia.RepositorioCompra
import gov.sefaz.labs.nosql.compras.core.servicos.ServicoCompra
import gov.sefaz.labs.nosql.compras.core.utils.GeradorDeDados
import gov.sefaz.labs.nosql.compras.mysql.norm.Application
import gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.repositorios.RepositorioCliente
import gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.repositorios.RepositorioProduto
import gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.repositorios.RepositorioUnidadeOperacional
import gov.sefaz.labs.nosql.compras.mysql.norm.tests.utils.TestUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.test.context.ContextConfiguration
import spock.lang.Ignore
import spock.lang.Specification

import java.text.SimpleDateFormat

@ContextConfiguration(classes = Application, loader = SpringApplicationContextLoader)
class ServicoCompraSpecification extends Specification {

    @Autowired
    ServicoCompra servicoCompra;

    @Autowired
    RepositorioCompra repositorioCompra;

    @Autowired
    RepositorioCliente repositorioCliente;

    @Autowired
    RepositorioProduto repositorioProduto;

    @Autowired
    RepositorioUnidadeOperacional repositorioUnidadeOperacional;

    void setup() {
        limpeBanco()
    }

    void cleanup() {
        limpeBanco()
    }

    void "insercao de um registro no banco"() {
        setup:
        servicoCompra.insira(compra)

        when: "os dados de uma compra foram enviados ao serviço de insercao"
        def compras = servicoCompra.liste()

        then: "a quantidade de compras na base deve ser igual a 1"
        compras.size() == 1

        where: "os dados da compra são aleatórios"
        geracaoDeDados = new GeradorDeDados()
        compra = geracaoDeDados.obtenhaCompraComDadosAleatorios()
    }

    void "consulta a um registro com base no ID"() {
        setup:
        def id = servicoCompra.insira(compra)

        when:
        def resultadoConsulta = servicoCompra.consulte(id)

        then:
        resultadoConsulta != null

        where:
        compra = TestUtils.obtenhaInstanciaCompra()
    }

    void "consulta à lista de compras com base em uma data"() {
        setup:
        compra1.setDataHora(data1)
        compra2.setDataHora(data1)
        compra3.setDataHora(data2)
        servicoCompra.insira(compra1)
        servicoCompra.insira(compra2)
        servicoCompra.insira(compra3)

        when:
        def resultadoConsulta = servicoCompra.listePorData(data1)

        then:
        resultadoConsulta.size() == 2

        where:
        dateFormat = new SimpleDateFormat("dd/MM/yyyy")
        data1 = dateFormat.parse("15/05/2014")
        data2 = dateFormat.parse("20/11/2013")
        compra1 = TestUtils.obtenhaInstanciaCompra()
        compra2 = TestUtils.obtenhaInstanciaCompra()
        compra3 = TestUtils.obtenhaInstanciaCompra()
    }

    void "consulta à lista completa de registros"() {
        setup:
        servicoCompra.insira(compra1)
        servicoCompra.insira(compra2)

        when:
        def resultadoConsulta = servicoCompra.liste()

        then:
        resultadoConsulta.size() == 2

        where:
        compra1 = TestUtils.obtenhaInstanciaCompra()
        compra2 = TestUtils.obtenhaInstanciaCompra()
    }

    void "alteração de um registro"() {
        setup:
        def id = servicoCompra.insira(compra)
        compra.setId(id)

        when:
        compra.setComentario(novoComentario)
        servicoCompra.atualize(compra)
        def resultadoConsulta = servicoCompra.consulte(id)

        then:
        resultadoConsulta.getComentario().equals(novoComentario)

        where:
        compra = TestUtils.obtenhaInstanciaCompra()
        novoComentario = "novo comentário"
    }

    void "exclusão de um registro"() {
        setup:
        def id = servicoCompra.insira(compra)

        when:
        def instanciaExclusao = new Compra()
        instanciaExclusao.setId(id)
        servicoCompra.exclua(instanciaExclusao)

        then:
        servicoCompra.liste().size() == 0

        where:
        compra = TestUtils.obtenhaInstanciaCompra()
    }

    @Ignore("Ignorado até solução para problema com a quantidade de linhas afetadas 'inesperada' pelo Hibernate")
    void "inserção em batch"() {
        when:
        servicoCompra.insiraEmLote(quantidadeDeRegistros, true)

        then:
        servicoCompra.liste().size() == 100

        where:
        quantidadeDeRegistros = 100L
    }

    private void limpeBanco() {
        repositorioCompra.excluaTodos();
        repositorioCliente.excluaTodos();
        repositorioProduto.excluaTodos();
        repositorioUnidadeOperacional.excluaTodos();
    }

}
