package gov.sefaz.labs.nosql.compras.core.tests.integracao.servicos;

import gov.sefaz.labs.nosql.compras.core.dominio.Compra;
import gov.sefaz.labs.nosql.compras.core.persistencia.RepositorioCompra;
import gov.sefaz.labs.nosql.compras.core.servicos.ServicoCompraImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ServicoCompraTests {

    @InjectMocks
    private ServicoCompraImpl servicoCompra;

    @Mock
    private RepositorioCompra repositorioCompra;

    private Compra compra;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(ServicoCompraTests.class);
    }

    @Test
    public void testInvocacaoRepositorioCompraInserir() throws Exception {
        servicoCompra.insira(compra);
        verify(repositorioCompra, times(1)).insira(compra);
    }
}
