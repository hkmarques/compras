package gov.sefaz.labs.nosql.compras.core.tests.utils;

import gov.sefaz.labs.nosql.compras.core.utils.TimeUUIDUtils;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class TimeUUIDUtilsTests {

    @Test
    public void testConversao() throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date dataQualquer = dateFormat.parse("30/01/2014");

        UUID convertido = TimeUUIDUtils.convertaDataParaUUID(dataQualquer);
        Date resultado = TimeUUIDUtils.getDateFromUUID(convertido);

        assertEquals(dataQualquer, resultado);
    }

}
