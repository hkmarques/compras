package gov.sefaz.labs.nosql.compras.core.tests.utils;

import gov.sefaz.labs.nosql.compras.core.dominio.Cliente;
import gov.sefaz.labs.nosql.compras.core.dominio.Produto;
import gov.sefaz.labs.nosql.compras.core.dominio.UnidadeOperacional;
import gov.sefaz.labs.nosql.compras.core.utils.GeradorDeDados;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class GeradorDeDadosTests {

    private GeradorDeDados geradorDeDados;

    @Before
    public void setUp() throws Exception {
        geradorDeDados = new GeradorDeDados();
    }

    @Test
    public void testGeracaoIntanciaCliente() throws Exception {
        Cliente cliente = geradorDeDados.obtenhaClienteComDadosAleatorios();
        assertNotNull(cliente.getId());
        assertNotNull(cliente.getNome());
    }

    @Test
    public void testGeracaoInstanciaProduto() throws Exception {
        Produto produto = geradorDeDados.obtenhaProdutoComDadosAleatorios();
        assertNotNull(produto.getId());
        assertNotNull(produto.getCategoria());
        assertNotNull(produto.getCodigo());
        assertNotNull(produto.getDescricao());
        assertNotNull(produto.getPrecoUnitario());
    }

    @Test
    public void testGeracaoInstanciaUnidadeOperacional() throws Exception {
        UnidadeOperacional unidadeOperacional = geradorDeDados.obtenhaUnidadeOperacionalComDadosAleatorios();
        assertNotNull(unidadeOperacional.getCodigo());
        assertNotNull(unidadeOperacional.getDescricao());
    }

}
