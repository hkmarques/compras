package gov.sefaz.labs.nosql.compras.core.persistencia;

import gov.sefaz.labs.nosql.compras.core.dominio.Compra;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface RepositorioCompra {

    UUID insira(Compra compra);

    Long insiraEmLote(long quantidadeDeRegistros, boolean valoresFixos);

    void atualize(Compra compra);

    void exclua(Compra compra);

    Compra consulte(UUID id);

    List<Compra> liste();

    List<Compra> listePorData(Date date);

    void excluaTodos();

}
