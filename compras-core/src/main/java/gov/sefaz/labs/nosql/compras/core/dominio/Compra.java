package gov.sefaz.labs.nosql.compras.core.dominio;

import java.util.Date;
import java.util.UUID;

public class Compra {

    protected UUID id;

    protected Date dataHora;

    protected String comentario;

    protected double quantidade;

    protected Cliente cliente;

    protected Produto produto;

    protected UnidadeOperacional unidadeOperacional;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Date getDataHora() {
        return dataHora;
    }

    public void setDataHora(Date dataHora) {
        this.dataHora = dataHora;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public UnidadeOperacional getUnidadeOperacional() {
        return unidadeOperacional;
    }

    public void setUnidadeOperacional(UnidadeOperacional unidadeOperacional) {
        this.unidadeOperacional = unidadeOperacional;
    }

    @Override
    public String toString() {
        return "Compra{" +
                "id=" + getId() +
                ", dataHora=" + getDataHora() +
                ", comentario='" + getComentario() + '\'' +
                ", quantidade=" + getQuantidade() +
                ", cliente=" + getCliente() +
                ", produto=" + getProduto() +
                ", unidadeOperacional=" + getUnidadeOperacional() +
                '}';
    }
}
