package gov.sefaz.labs.nosql.compras.core.servicos;

import gov.sefaz.labs.nosql.compras.core.dominio.Compra;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface ServicoCompra {

    UUID insira(Compra compra);

    void atualize(Compra compra);

    void exclua(Compra compra);

    Compra consulte(UUID id);

    List<Compra> liste();

    List<Compra> listePorData(Date data);

    void insiraEmLote(long quantidadeDeRegistros, boolean valoresFixos);

}
