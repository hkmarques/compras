package gov.sefaz.labs.nosql.compras.core.dominio;

public enum EnumCategoriaProduto {

    ALIMENTO,

    ROUPA,

    ELETRODOMESTICO

}
