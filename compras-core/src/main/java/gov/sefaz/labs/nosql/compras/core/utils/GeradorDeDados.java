package gov.sefaz.labs.nosql.compras.core.utils;

import gov.sefaz.labs.nosql.compras.core.dominio.*;
import org.fluttercode.datafactory.impl.DataFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class GeradorDeDados {

    private final List<EnumCategoriaProduto> categoriasProduto = new ArrayList<EnumCategoriaProduto>(Arrays.asList(EnumCategoriaProduto.values()));

    private final List<String> produtos = Arrays.asList("Chocolate", "Água Mineral", "Produto qualquer");

    private DataFactory dataFactory = new DataFactory();

    /**
     * Preenche uma instância de <code>Compra</code> com valores aleatórios.
     *
     * @param compra    a instância de <code>Compra</code> a ser preenchida.
     */
    public void definaValoresAleatorios(Compra compra) {
        preenchaDataHora(compra);
        compra.setComentario(dataFactory.getRandomText(50));
        compra.setQuantidade(dataFactory.getNumberBetween(1, 20));

        compra.setCliente(this.obtenhaClienteComDadosAleatorios());
        compra.setProduto(this.obtenhaProdutoComDadosAleatorios());
        compra.setUnidadeOperacional(this.obtenhaUnidadeOperacionalComDadosAleatorios());
    }

    /**
     * Preenche uma instância de <code>Compra</code> com valores fixos..
     *
     * @param compra    a instância de <code>Compra</code> a ser preenchida.
     */
    public void definaValoresFixos(Compra compra) {
        preenchaDataHora(compra);
        compra.setComentario("Comentário fixo");
        compra.setQuantidade(5);

        Cliente cliente = new Cliente();
        cliente.setNome("Tadeu Gomes");
        compra.setCliente(cliente);

        Produto produto = new Produto();
        produto.setDescricao("Água Mineral");
        produto.setCategoria(EnumCategoriaProduto.ALIMENTO);
        produto.setCodigo(1);
        produto.setPrecoUnitario(4.5);
        compra.setProduto(produto);

        UnidadeOperacional unidadeOperacional = new UnidadeOperacional();
        unidadeOperacional.setCodigo(1);
        unidadeOperacional.setDescricao("Loja Principal");
        compra.setUnidadeOperacional(unidadeOperacional);
    }

    /**
     * Obtém uma instância de <code>Cliente</code> preenchida com dados aleatórios.
     *
     * @return uma instância de <code>Cliente</code> preenchida com dados aleatórios.
     */
    public Cliente obtenhaClienteComDadosAleatorios() {
        Cliente cliente = new Cliente();
        cliente.setId(UUID.randomUUID());
        cliente.setNome(dataFactory.getName());
        return cliente;
    }

    /**
     * Obtém uma instãncia de <code>Compra</code> preenchida com dados aleatórios.
     * Obs.: Os id para produto, cliente e compra são individuais.
     *
     * @return uma instância de <code>Compra</code> preenchida com dados aleatórios
     */
    public Compra obtenhaCompraComDadosAleatorios() {
        Compra compra = new Compra();

        compra.setCliente(this.obtenhaClienteComDadosAleatorios());
        compra.setProduto(this.obtenhaProdutoComDadosAleatorios());
        compra.setUnidadeOperacional(this.obtenhaUnidadeOperacionalComDadosAleatorios());

        Date dataMinima = dataFactory.getDate(2001, 1, 1);
        Date dataMaxima = dataFactory.getDate(2014, 8, 10);
        Date data = dataFactory.getDateBetween(dataMinima, dataMaxima);

        compra.setId(TimeUUIDUtils.convertaDataParaUUID(data));
        compra.setComentario(dataFactory.getRandomText(1, 10));

        compra.setDataHora(data);
        compra.setQuantidade(dataFactory.getNumberBetween(1, 10));

        return compra;
    }

    /**
     * Obtém uma instãncia de <code>Produto</code> preenchida com dados aleatórios.
     *
     * @return uma instância de <code>Produto</code> preenchida com dados aleatórios
     */
    public Produto obtenhaProdutoComDadosAleatorios() {
        Produto produto = new Produto();
        produto.setId(UUID.randomUUID());
        produto.setCategoria(dataFactory.getItem(categoriasProduto));
        produto.setCodigo(dataFactory.getNumberUpTo(1000000));
        String descProduto = dataFactory.getItem(produtos) + " " + dataFactory.getRandomChars(4).toUpperCase();
        produto.setDescricao(descProduto);
        produto.setPrecoUnitario(dataFactory.getNumberBetween(1, 200));
        return produto;
    }

    /**
     * Obtém uma instãncia de <code>UnidadeOperacional</code> preenchida com dados aleatórios.
     *
     * @return uma instância de <code>UnidadeOperacional</code> preenchida com dados aleatórios
     */
    public UnidadeOperacional obtenhaUnidadeOperacionalComDadosAleatorios() {
        UnidadeOperacional unidadeOperacional = new UnidadeOperacional();
        unidadeOperacional.setCodigo(dataFactory.getNumberUpTo(100000));
        unidadeOperacional.setDescricao(dataFactory.getBusinessName());
        return unidadeOperacional;
    }

    /**
     * Preenche a data/hora para uma instância de <code>Compra</code> com base
     * em um intervalo predefinido.
     * @param compra    a instância de <code>Compra</code> a ser preenchida.
     */
    private void preenchaDataHora(Compra compra) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date dataMinima = null;
        Date dataMaxima = new Date();

        try {
            dataMinima = dateFormat.parse("01/01/2000");
        } catch (ParseException e) {
            System.out.println("Erro ao converter data: " + e.getMessage());
            e.printStackTrace();
        }

        compra.setDataHora(dataFactory.getDateBetween(dataMinima, dataMaxima));
    }

}
