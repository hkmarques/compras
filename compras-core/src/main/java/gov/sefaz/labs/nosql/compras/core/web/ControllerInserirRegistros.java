package gov.sefaz.labs.nosql.compras.core.web;

import gov.sefaz.labs.nosql.compras.core.dominio.Compra;
import gov.sefaz.labs.nosql.compras.core.servicos.ServicoCompra;
import gov.sefaz.labs.nosql.compras.core.utils.GeradorDeDados;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Controller
public class ControllerInserirRegistros {

    @Autowired
    private ServicoCompra servicoCompra;

    @Autowired
    private ServicoWrapperAsync servicoAsync;

    @RequestMapping(value = "/insiraBatch")
    public @ResponseBody String insiraRegistros(
            @RequestParam(value = "registros", required = true) Long registros,
            @RequestParam(value = "step", required = false, defaultValue = "5000") Long step,
            @RequestParam(value = "valoresFixos", required = false, defaultValue = "false") boolean valoresFixos) throws InterruptedException {
        return populeCFComprasDia(registros, step, valoresFixos);
    }

    @RequestMapping(value = "/insira")
    public @ResponseBody String insira() throws InterruptedException, ExecutionException {
        Compra compra = new GeradorDeDados().obtenhaCompraComDadosAleatorios();
        UUID id = servicoCompra.insira(compra);
        return String.format("Registro = %s\n", id);
    }

    @RequestMapping(value = "/atualize")
    public @ResponseBody String atualize() {
        throw new UnsupportedOperationException("Ação não implementada");   // TODO: implementar atualização no controller
    }

    @RequestMapping(value = "/consulte")
    public @ResponseBody Compra consulte(@RequestParam(value = "id") String id) throws InterruptedException, ExecutionException {
        UUID uuid = UUID.fromString(id);
        return servicoCompra.consulte(uuid);
    }

    @RequestMapping(value = "/listePorData")
    public @ResponseBody List<Compra> listePorData(@RequestParam(value = "data") String data) throws InterruptedException, ExecutionException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        data = data.replaceAll("-", "/");

        Date objDate;
        try {
            objDate = dateFormat.parse(data);
        } catch (ParseException e) {
            e.printStackTrace();
            return new ArrayList<Compra>();
        }

        return servicoCompra.listePorData(objDate);
    }

    @RequestMapping(value = "/exclua")
    public @ResponseBody String exclua(@RequestParam(value = "id") String id) {
        UUID uuid = UUID.fromString(id);
        Compra compra = new Compra();
        compra.setId(uuid);
        servicoCompra.exclua(compra);

        return String.format("Compra %s excluída\n", uuid);
    }

    @RequestMapping(value = "/liste")
    public @ResponseBody List<Compra> liste() throws InterruptedException, ExecutionException {
        return servicoCompra.liste();
    }

    /**
     * Método responsável por distribuir uma operação de inserção em batch em várias chamadas (assíncronas) ao serviço.
     *
     * @param quantidadeAInserir    a quantidade total de registros a serem inseridos nesta operaçao.
     * @param step                  a quantidade de registros a serem inseridos atomicamente (o número total será quebrado em n grupos,
     *                              cada um com esta quantidade de registros).
     * @param valoresFixos          indica se os registos a serem inseridos devem ser preenchidos com dados fixos ou aleatórios.
     * @return Uma mensagem final para a execução (ou falha) da operação.
     * @throws InterruptedException caso a thread seja interrompida.
     */
    private String populeCFComprasDia(long quantidadeAInserir, long step, boolean valoresFixos) throws InterruptedException {
        long start = System.currentTimeMillis();
        System.out.println("\n");

        long quantidadeBatch = step > 0 ? step : 10000;

        long batchesCheios = quantidadeAInserir / step;
        long sobras = quantidadeAInserir % step;

        List<Future<Void>> requisicoes = new ArrayList<Future<Void>>();
        for (int i = 0; i < batchesCheios; i++) {
            System.out.printf("\nIniciando nova thread para inserção de %d registros.\n", quantidadeBatch);
            requisicoes.add(servicoAsync.insiraEmLote(quantidadeBatch, valoresFixos));
        }

        if (sobras > 0) {
            System.out.printf("\nIniciando nova thread para inserção de %d registros.\n", sobras);
            requisicoes.add(servicoAsync.insiraEmLote(sobras, valoresFixos));
        }

        boolean todasFinalizadas = false;
        do {
            Thread.sleep(10);   // Evitando espera ocupada
            for (Future<Void> requisicao : requisicoes) {
                if (!requisicao.isDone()) {
                    todasFinalizadas = false;
                    break;
                }

                todasFinalizadas = true;
            }
        } while (!todasFinalizadas);

        long duracaoTotal = System.currentTimeMillis() - start;
        String resposta = String.format("\nDuracao total: %.3f s\n\n", duracaoTotal / 1000.0);

        System.out.printf(resposta);

        return resposta;
    }

}
