package gov.sefaz.labs.nosql.compras.core.utils;

import java.util.Date;
import java.util.UUID;

/**
 * Classe agrupando funções utilitárias para tratamento de TimeUUID's encontradas em fontes diversas.
 */
public class TimeUUIDUtils {

    // This method comes from Hector's TimeUUIDUtils class:
    // https://github.com/rantav/hector/blob/master/core/src/main/java/me/...
    static final long NUM_100NS_INTERVALS_SINCE_UUID_EPOCH = 0x01b21dd213814000L;

    /**
     * Código extraído da <a href='http://wiki.apache.org/cassandra/FAQ#working_with_timeuuid_in_java'>documentação oficial da Apache</a>.
     *
     * @param data a data a ser convertida
     * @return um UUID tipo 1 correspondente à data informada
     * @see <a href="http://wiki.apache.org/cassandra/TimeBaseUUIDNotes">http://wiki.apache.org/cassandra/TimeBaseUUIDNotes</a>
     */
    public static UUID convertaDataParaUUID(Date data) {
        long origTime = data.getTime();
        long time = origTime * 10000 + NUM_100NS_INTERVALS_SINCE_UUID_EPOCH;
        long timeLow = time &       0xffffffffL;
        long timeMid = time &   0xffff00000000L;
        long timeHi = time & 0xfff000000000000L;
        long upperLong = (timeLow << 32) | (timeMid >> 16) | (1 << 12) | (timeHi >> 48) ;
        return new UUID(upperLong, 0xC000000000000000L);
    }

    public static long getTimeFromUUID(UUID uuid) {
        return (uuid.timestamp() - NUM_100NS_INTERVALS_SINCE_UUID_EPOCH) / 10000;
    }

    public static Date getDateFromUUID(UUID uuid) {
        return new Date(getTimeFromUUID(uuid));
    }

}
