package gov.sefaz.labs.nosql.compras.core.dominio;

import java.util.UUID;

public class Produto {

    private UUID id;

    private String descricao;

    private EnumCategoriaProduto categoria;

    private long codigo;

    private double precoUnitario;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public EnumCategoriaProduto getCategoria() {
        return categoria;
    }

    public void setCategoria(EnumCategoriaProduto categoria) {
        this.categoria = categoria;
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public double getPrecoUnitario() {
        return precoUnitario;
    }

    public void setPrecoUnitario(double precoUnitario) {
        this.precoUnitario = precoUnitario;
    }

    @Override
    public String toString() {
        return "Produto{" +
                "id=" + id +
                ", descricao='" + descricao + '\'' +
                ", categoria=" + categoria +
                ", codigo=" + codigo +
                ", precoUnitario=" + precoUnitario +
                '}';
    }
}
