package gov.sefaz.labs.nosql.compras.core.servicos;

import gov.sefaz.labs.nosql.compras.core.dominio.Compra;
import gov.sefaz.labs.nosql.compras.core.persistencia.RepositorioCompra;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service("servicoCompra")
public class ServicoCompraImpl implements ServicoCompra {

    private RepositorioCompra repositorioCompra;

    @Autowired
    @SuppressWarnings("SpringJavaAutowiringInspection")
    public ServicoCompraImpl(RepositorioCompra repositorioCompra) {
        this.repositorioCompra = repositorioCompra;
    }

    @Override
    public UUID insira(Compra compra) {
        return repositorioCompra.insira(compra);
    }

    @Override
    public void atualize(Compra compra) {
        Compra compraPersistida = repositorioCompra.consulte(compra.getId());
        BeanUtils.copyProperties(compra, compraPersistida, "id", "cliente", "produto", "unidadeOperacional");
        compraPersistida.setCliente(compra.getCliente());
        compraPersistida.setProduto(compra.getProduto());
        compraPersistida.setUnidadeOperacional(compra.getUnidadeOperacional());
        repositorioCompra.atualize(compraPersistida);
    }

    @Override
    public void exclua(Compra compra) {
        repositorioCompra.exclua(compra);
    }

    @Override
    public Compra consulte(UUID id) {
        return repositorioCompra.consulte(id);
    }

    @Override
    public List<Compra> liste() {
        return repositorioCompra.liste();
    }

    @Override
    public List<Compra> listePorData(Date data) {
        return repositorioCompra.listePorData(data);
    }

    @Override
    public void insiraEmLote(long quantidadeDeRegistros, boolean valoresFixos) {
        repositorioCompra.insiraEmLote(quantidadeDeRegistros, valoresFixos);
    }
}
