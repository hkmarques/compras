package gov.sefaz.labs.nosql.compras.core.web;

import gov.sefaz.labs.nosql.compras.core.servicos.ServicoCompra;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;

@Service
public class ServicoWrapperAsync {

    @Autowired
    private ServicoCompra servicoCompra;

    @Async
    public Future<Void> insiraEmLote(long quantidadeDeRegistros, boolean valoresFixos) {
        servicoCompra.insiraEmLote(quantidadeDeRegistros, valoresFixos);
        return new AsyncResult<Void>(null);
    }

}
