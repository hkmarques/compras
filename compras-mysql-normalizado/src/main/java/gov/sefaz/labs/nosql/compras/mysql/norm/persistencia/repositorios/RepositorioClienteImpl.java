package gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.repositorios;

import gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.entidades.Cliente;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Scope("prototype")
@Repository("repositorioCliente")
public class RepositorioClienteImpl implements RepositorioCliente {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public UUID insira(Cliente cliente) {
        getCurrentSession().save(cliente);
        return cliente.getId();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void atualize(Cliente cliente) {
        getCurrentSession().merge(cliente);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void exclua(Cliente cliente) {
        Session sessao = getCurrentSession();
        Cliente objetoPersistido;
        if ((objetoPersistido = (Cliente) sessao.get(Cliente.class, cliente.getId())) != null) {
            sessao.delete(objetoPersistido);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Cliente consulte(UUID id) {
        return (Cliente) getCurrentSession().get(Cliente.class, id != null ? id.toString() : "");
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public List<Cliente> liste() {
        return getCurrentSession().createQuery("from Cliente").list();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void excluaTodos() {
        getCurrentSession().createQuery("delete from Cliente").executeUpdate();
    }

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

}
