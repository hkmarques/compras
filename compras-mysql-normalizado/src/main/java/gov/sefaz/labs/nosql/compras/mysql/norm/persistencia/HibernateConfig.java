package gov.sefaz.labs.nosql.compras.mysql.norm.persistencia;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
public class HibernateConfig {

    @Resource(name = "dataSourceProperties")
    Properties dsProperties;

    @Resource(name = "persistenceProperties")
    Properties persistenceProperties;

    @Bean(name = "hibernateProperties")
    public PropertiesFactoryBean hibernateProperties() {
        PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setLocation(new ClassPathResource("hibernate.properties"));
        return propertiesFactoryBean;
    }

    @Bean(name = "dataSourceProperties")
    public PropertiesFactoryBean dataSourceProperties() {
        PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setLocation(new ClassPathResource("dataSource.properties"));
        return propertiesFactoryBean;
    }

    @Bean(name = "persistenceProperties")
    public PropertiesFactoryBean persistenceProperties() {
        PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setLocation(new ClassPathResource("persistence.properties"));
        return propertiesFactoryBean;
    }

    @Bean
    public DataSource dataSource() throws IOException {
        final HikariDataSource dataSource = new HikariDataSource();
        dataSource.setMaximumPoolSize(100);
        dataSource.setDataSourceClassName(persistenceProperties.getProperty("dataSource.className"));
        dataSource.setDataSourceProperties(dsProperties);

        return dataSource;
    }

    @Bean(name = "sessionFactory")
    public LocalSessionFactoryBean sessionFactory() throws IOException {
        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
        sessionFactoryBean.setDataSource(dataSource());
        sessionFactoryBean.setPackagesToScan(persistenceProperties.getProperty("packagesToScan"));

        return sessionFactoryBean;
    }

    @Bean
    public HibernateTransactionManager transactionManager() throws IOException {
        return new HibernateTransactionManager(sessionFactory().getObject());
    }

}
