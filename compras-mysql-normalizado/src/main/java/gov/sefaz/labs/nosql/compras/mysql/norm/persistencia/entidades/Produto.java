package gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.entidades;

import gov.sefaz.labs.nosql.compras.core.dominio.EnumCategoriaProduto;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "PRODUTO")
public class Produto extends gov.sefaz.labs.nosql.compras.core.dominio.Produto implements Serializable {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "PROD_ID", unique = true, length = 36)
    private String uuid;

    @Column(name = "PROD_DESCRICAO")
    private String descricao;

    @Column(name = "PROD_CATEGORIA")
    private EnumCategoriaProduto categoria;

    @Column(name = "PROD_CODIGO")
    private long codigo;

    @Column(name = "PROD_PRECO_UNIT")
    private double precoUnitario;

    public Produto() {
    }

    public Produto(gov.sefaz.labs.nosql.compras.core.dominio.Produto produto) {
        this();
        BeanUtils.copyProperties(produto, this, "id");
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public UUID getId() {
        return uuid != null
               ? UUID.fromString(uuid)
               : null;
    }

    @Override
    public void setId(UUID id) {
        this.uuid = id.toString();
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public EnumCategoriaProduto getCategoria() {
        return categoria;
    }

    public void setCategoria(EnumCategoriaProduto categoria) {
        this.categoria = categoria;
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public double getPrecoUnitario() {
        return precoUnitario;
    }

    public void setPrecoUnitario(double precoUnitario) {
        this.precoUnitario = precoUnitario;
    }
}
