package gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.entidades;

import gov.sefaz.labs.nosql.compras.core.dominio.Compra;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "COMPRA")
public class CompraPorDiaMySQLNormalizada extends Compra implements Serializable {

    @Id
    @Column(name = "COMP_ID", length = 36, unique = true)
    private String uuid;

    @Column(name = "COMP_DATA")
    private Date data;

    @Column(name = "COMP_COMENTARIO")
    private String comentario;

    @Column(name = "COMP_QUANTIDADE")
    private double quantidade;

    @ManyToOne
    @JoinColumn(name = "COMP_ID_CLIENTE")
    private Cliente cliente;

    @ManyToOne
    @JoinColumn(name = "COMP_ID_PRODUTO")
    private Produto produto;

    @ManyToOne
    @JoinColumn(name = "COMP_ID_UNIDADE_OPERACIONAL")
    private UnidadeOperacional unidadeOperacional;

    public CompraPorDiaMySQLNormalizada() {
    }

    public CompraPorDiaMySQLNormalizada(Date data, Cliente cliente, Produto produto) {
        this();
        this.data = data;
        this.cliente = cliente;
        this.produto = produto;
    }

    public CompraPorDiaMySQLNormalizada(Compra compra) {
        this();
        BeanUtils.copyProperties(compra, this, "cliente", "produto", "unidadeOperacional", "dataHora", "id");
        this.setProduto(compra.getProduto());
        this.setCliente(compra.getCliente());
        this.setUnidadeOperacional(compra.getUnidadeOperacional());
        Date dataHora = compra.getDataHora();
        this.setDataHora(dataHora);
        this.setId(compra.getId());
    }

    @Override
    public UUID getId() {
        return UUID.fromString(uuid);
    }

    @Override
    public void setId(UUID id) {
        this.uuid = id != null
            ? id.toString()
            : null;
    }

    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public void setCliente(gov.sefaz.labs.nosql.compras.core.dominio.Cliente cliente) {
        this.cliente = new Cliente(cliente);
    }

    public Produto getProduto() {
        return this.produto;
    }

    @Override
    public void setProduto(gov.sefaz.labs.nosql.compras.core.dominio.Produto produto) {
        this.produto = new Produto(produto);
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public UnidadeOperacional getUnidadeOperacional() {
        return this.unidadeOperacional;
    }

    public void setUnidadeOperacional(UnidadeOperacional unidadeOperacional) {
        this.unidadeOperacional = unidadeOperacional;
    }

    @Override
    public void setUnidadeOperacional(gov.sefaz.labs.nosql.compras.core.dominio.UnidadeOperacional unidadeOperacional) {
        this.unidadeOperacional = new UnidadeOperacional(unidadeOperacional);
    }

    @Override
    public String getComentario() {
        return comentario;
    }

    @Override
    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    @Override
    public double getQuantidade() {
        return quantidade;
    }

    @Override
    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    @Override
    public Date getDataHora() {
        return data;
    }

    @Override
    public void setDataHora(Date data) {
        this.data = data;
    }
}
