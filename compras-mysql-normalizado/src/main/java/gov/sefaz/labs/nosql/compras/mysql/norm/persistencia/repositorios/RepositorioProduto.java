package gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.repositorios;

import gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.entidades.Produto;

import java.util.List;
import java.util.UUID;

public interface RepositorioProduto {

    UUID insira(Produto produto);

    void atualize(Produto produto);

    void exclua(Produto produto);

    Produto consulte(UUID id);

    List<Produto> liste();

    void excluaTodos();

}
