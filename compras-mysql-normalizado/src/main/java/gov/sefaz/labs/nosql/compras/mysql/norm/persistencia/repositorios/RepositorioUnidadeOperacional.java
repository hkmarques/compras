package gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.repositorios;

import gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.entidades.UnidadeOperacional;

import java.util.List;

public interface RepositorioUnidadeOperacional {

    Long insira(UnidadeOperacional unidadeOperacional);

    void atualize(UnidadeOperacional unidadeOperacional);

    void exclua(UnidadeOperacional unidadeOperacional);

    UnidadeOperacional consulte(Long codigo);

    List<UnidadeOperacional> liste();

    void excluaTodos();

}
