package gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.repositorios;

import gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.entidades.Cliente;

import java.util.List;
import java.util.UUID;

public interface RepositorioCliente {

    UUID insira(Cliente cliente);

    void atualize(Cliente cliente);

    void exclua(Cliente cliente);

    Cliente consulte(UUID id);

    List<Cliente> liste();

    void excluaTodos();

}
