package gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.repositorios;

import gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.entidades.UnidadeOperacional;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Scope("prototype")
@Repository("repositorioUnidadeOperacional")
public class RepositorioUnidadeOperacionalImpl implements RepositorioUnidadeOperacional {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Long insira(UnidadeOperacional unidadeOperacional) {
        getCurrentSession().save(unidadeOperacional);
        return unidadeOperacional.getCodigo();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void atualize(UnidadeOperacional unidadeOperacional) {
        assert UnidadeOperacional.class.isInstance(unidadeOperacional);
        getCurrentSession().merge(unidadeOperacional);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void exclua(UnidadeOperacional unidadeOperacional) {
        Session sessao = getCurrentSession();
        UnidadeOperacional objetoPersistido;
        if ((objetoPersistido = (UnidadeOperacional) sessao.get(UnidadeOperacional.class, unidadeOperacional.getCodigo())) != null) {
            sessao.delete(objetoPersistido);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public UnidadeOperacional consulte(Long codigo) {
        return (UnidadeOperacional) getCurrentSession().get(UnidadeOperacional.class, codigo);
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public List<UnidadeOperacional> liste() {
        return getCurrentSession().createQuery("from UnidadeOperacional").list();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void excluaTodos() {
        getCurrentSession().createQuery("delete from UnidadeOperacional").executeUpdate();
    }

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

}
