package gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.repositorios;

import gov.sefaz.labs.nosql.compras.core.dominio.Compra;
import gov.sefaz.labs.nosql.compras.core.persistencia.RepositorioCompra;
import gov.sefaz.labs.nosql.compras.core.utils.GeradorDeDados;
import gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.entidades.Cliente;
import gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.entidades.CompraPorDiaMySQLNormalizada;
import gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.entidades.Produto;
import gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.entidades.UnidadeOperacional;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Scope("prototype")
@Repository("repositorioCompra")
public class RepositorioCompraImpl implements RepositorioCompra {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private RepositorioProduto repositorioProduto;

    @Autowired
    private RepositorioCliente repositorioCliente;

    @Autowired
    private RepositorioUnidadeOperacional repositorioUnidadeOperacional;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public UUID insira(Compra compra) {
        CompraPorDiaMySQLNormalizada compraMySQL = prepareCompraParaInsercao(compra);
        getCurrentSession().save(compraMySQL);
        return compraMySQL.getId();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void atualize(Compra compra) {
        assert CompraPorDiaMySQLNormalizada.class.isInstance(compra) : "A instância de compra recebida não é uma instância de CompraPorDiaMySQLNormalizada";
        CompraPorDiaMySQLNormalizada compraPorDia = (CompraPorDiaMySQLNormalizada) compra;
        Cliente clienteConsultado = repositorioCliente.consulte(compraPorDia.getCliente().getId());
        if (clienteConsultado == null) {
            UUID idNovoCliente = repositorioCliente.insira(compraPorDia.getCliente());
            clienteConsultado = repositorioCliente.consulte(idNovoCliente);
        }

        compraPorDia.setCliente(clienteConsultado);

        Produto produtoConsultado = repositorioProduto.consulte(compraPorDia.getProduto().getId());
        if (produtoConsultado == null) {
            UUID idNovoProduto = repositorioProduto.insira(compraPorDia.getProduto());
            produtoConsultado = repositorioProduto.consulte(idNovoProduto);
        }

        compraPorDia.setProduto(produtoConsultado);


        UnidadeOperacional unidOperacionalConsultada = repositorioUnidadeOperacional.consulte(compraPorDia.getUnidadeOperacional().getCodigo());
        if (unidOperacionalConsultada == null) {
            Long codigoNovaUnidade = repositorioUnidadeOperacional.insira(compraPorDia.getUnidadeOperacional());
            unidOperacionalConsultada = repositorioUnidadeOperacional.consulte(codigoNovaUnidade);
        }

        compraPorDia.setUnidadeOperacional(unidOperacionalConsultada);

        getCurrentSession().merge(compraPorDia);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void exclua(Compra compra) {
        Session sessao = getCurrentSession();
        CompraPorDiaMySQLNormalizada objetoPersistido;
        if ((objetoPersistido = (CompraPorDiaMySQLNormalizada) sessao.get(CompraPorDiaMySQLNormalizada.class, compra.getId().toString())) != null) {
            sessao.delete(objetoPersistido);
        }
    }

    @Override
    public Long insiraEmLote(long quantidadeDeRegistros, boolean valoresFixos) {
        GeradorDeDados geradorDeDados = new GeradorDeDados();

        Compra dadosQuaisquer = geradorDeDados.obtenhaCompraComDadosAleatorios();
        Produto produtoQualquer = new Produto(dadosQuaisquer.getProduto());
        Cliente clienteQualquer = new Cliente(dadosQuaisquer.getCliente());
        UnidadeOperacional unidadeQualquer = new UnidadeOperacional(dadosQuaisquer.getUnidadeOperacional());

        UUID idCliente = repositorioCliente.insira(clienteQualquer);
        UUID idProduto = repositorioProduto.insira(produtoQualquer);
        Long codigoUnidade = repositorioUnidadeOperacional.insira(unidadeQualquer);

        clienteQualquer = repositorioCliente.consulte(idCliente);
        produtoQualquer = repositorioProduto.consulte(idProduto);
        unidadeQualquer = repositorioUnidadeOperacional.consulte(codigoUnidade);

        Session session = sessionFactory.openSession();
        Long start = System.nanoTime();
        for (int i = 0; i < quantidadeDeRegistros; i++) {
            CompraPorDiaMySQLNormalizada compra = new CompraPorDiaMySQLNormalizada();
            if (valoresFixos) {
                geradorDeDados.definaValoresFixos(compra);
            } else {
                geradorDeDados.definaValoresAleatorios(compra);
            }

            compra.setCliente(clienteQualquer);
            compra.setProduto(produtoQualquer);
            compra.setUnidadeOperacional(unidadeQualquer);
            compra.setId(UUID.randomUUID());
            session.save(compra);
        }

        session.flush();
        session.clear();
        long duracao = System.nanoTime() - start;
        session.close();
        return duracao / 1000000;
    }

    @Override
    @Transactional(readOnly = true)
    public Compra consulte(UUID id) {
        return (Compra) getCurrentSession().get(CompraPorDiaMySQLNormalizada.class, id.toString());
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public List<Compra> liste() {
        return getCurrentSession().createQuery("from CompraPorDiaMySQLNormalizada").list();
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public List<Compra> listePorData(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        Calendar apenasData = Calendar.getInstance();
        apenasData.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), 0, 0, 0);
        Calendar dataSeguinte = Calendar.getInstance();
        dataSeguinte.setTime(apenasData.getTime());
        dataSeguinte.add(Calendar.DATE, 1);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String query = String.format("from CompraPorDiaMySQLNormalizada where data between '%s' and '%s'",
                dateFormat.format(apenasData.getTime()),
                dateFormat.format(dataSeguinte.getTime()));

        return getCurrentSession().createQuery(query).list();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void excluaTodos() {
        getCurrentSession().createQuery("delete from CompraPorDiaMySQLNormalizada").executeUpdate();
    }

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    public void setRepositorioProduto(RepositorioProduto repositorioProduto) {
        this.repositorioProduto = repositorioProduto;
    }

    public void setRepositorioCliente(RepositorioCliente repositorioCliente) {
        this.repositorioCliente = repositorioCliente;
    }

    public void setRepositorioUnidadeOperacional(RepositorioUnidadeOperacional repositorioUnidadeOperacional) {
        this.repositorioUnidadeOperacional = repositorioUnidadeOperacional;
    }

    private CompraPorDiaMySQLNormalizada prepareCompraParaInsercao(Compra compra) {
        CompraPorDiaMySQLNormalizada compraMySQL = new CompraPorDiaMySQLNormalizada(compra);

        gov.sefaz.labs.nosql.compras.core.dominio.Produto produto = compra.getProduto();
        assert null != compra.getProduto();
        gov.sefaz.labs.nosql.compras.core.dominio.Cliente cliente = compra.getCliente();
        assert null != compra.getCliente();
        gov.sefaz.labs.nosql.compras.core.dominio.UnidadeOperacional unidadeOperacional = compra.getUnidadeOperacional();
        assert null != compra.getUnidadeOperacional();

        Produto produtoPersistido = repositorioProduto.consulte(produto.getId());
        if (produtoPersistido == null) {
            produtoPersistido = new Produto(produto);
            repositorioProduto.insira(produtoPersistido);
        }

        compraMySQL.setProduto(produtoPersistido);

        Cliente clientePersistido = repositorioCliente.consulte(cliente.getId());
        if (clientePersistido == null) {
            clientePersistido = new Cliente(cliente);
            repositorioCliente.insira(clientePersistido);
        }

        compraMySQL.setCliente(clientePersistido);

        UnidadeOperacional unidadeOperacionalPersistida = repositorioUnidadeOperacional.consulte(unidadeOperacional.getCodigo());
        if (unidadeOperacionalPersistida == null) {
            unidadeOperacionalPersistida = new UnidadeOperacional(unidadeOperacional);
            repositorioUnidadeOperacional.insira(unidadeOperacionalPersistida);
        }

        compraMySQL.setUnidadeOperacional(unidadeOperacionalPersistida);
        return compraMySQL;
    }
}
