package gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.entidades;

import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "UNIDADE_OPERACIONAL")
public class UnidadeOperacional extends gov.sefaz.labs.nosql.compras.core.dominio.UnidadeOperacional implements Serializable {

    @Id
    @Column(name = "UNOP_ID", unique = true)
    @SequenceGenerator(name="unidOp", sequenceName="unidOp_seq", allocationSize = 1)
    private long codigo;

    @Column(name = "UNOP_DESCRICAO")
    private String descricao;

    public UnidadeOperacional() {
    }

    public UnidadeOperacional(gov.sefaz.labs.nosql.compras.core.dominio.UnidadeOperacional unidadeOperacional) {
        this();
        BeanUtils.copyProperties(unidadeOperacional, this, "id");
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
