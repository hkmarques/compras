package gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.repositorios;

import gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.entidades.Produto;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Scope("prototype")
@Repository("repositorioProduto")
public class RepositorioProdutoImpl implements RepositorioProduto {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public UUID insira(Produto produto) {
        getCurrentSession().save(produto);
        return produto.getId();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void atualize(Produto produto) {
        assert Produto.class.isInstance(produto);
        getCurrentSession().merge(produto);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void exclua(Produto produto) {
        Session sessao = getCurrentSession();
        Produto objetoPersistido;
        if ((objetoPersistido = (Produto) sessao.get(Produto.class, produto.getId())) != null) {
            sessao.delete(objetoPersistido);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Produto consulte(UUID id) {
        return (Produto) getCurrentSession().get(Produto.class, id != null ? id.toString() : "");
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public List<Produto> liste() {
        return getCurrentSession().createQuery("from Produto").list();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void excluaTodos() {
        getCurrentSession().createQuery("delete from Produto").executeUpdate();
    }

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

}
