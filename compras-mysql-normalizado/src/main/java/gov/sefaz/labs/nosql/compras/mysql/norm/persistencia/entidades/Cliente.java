package gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.entidades;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "CLIENTE")
public class Cliente extends gov.sefaz.labs.nosql.compras.core.dominio.Cliente implements Serializable {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "CLIE_ID", unique = true, length = 36)
    private String uuid;

    @Column(name = "CLIE_NOME")
    private String nome;

    public Cliente() {
    }

    public Cliente(gov.sefaz.labs.nosql.compras.core.dominio.Cliente cliente) {
        this();
        BeanUtils.copyProperties(cliente, this, "id");
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public UUID getId() {
        return uuid != null
               ? UUID.fromString(uuid)
               : null;
    }

    @Override
    public void setId(UUID id) {
        this.uuid = id.toString();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
