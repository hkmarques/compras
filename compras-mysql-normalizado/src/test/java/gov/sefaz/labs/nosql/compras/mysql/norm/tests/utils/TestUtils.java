package gov.sefaz.labs.nosql.compras.mysql.norm.tests.utils;

import gov.sefaz.labs.nosql.compras.core.dominio.Compra;
import gov.sefaz.labs.nosql.compras.core.utils.GeradorDeDados;

public class TestUtils {

    public static Compra obtenhaInstanciaCompra() {
        return new GeradorDeDados().obtenhaCompraComDadosAleatorios();
    }

}
