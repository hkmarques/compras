package gov.sefaz.labs.nosql.compras.mysql.norm.tests.unidade.persistencia;

import gov.sefaz.labs.nosql.compras.core.dominio.Compra;
import gov.sefaz.labs.nosql.compras.core.dominio.EnumCategoriaProduto;
import gov.sefaz.labs.nosql.compras.core.dominio.Produto;
import gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.entidades.Cliente;
import gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.entidades.CompraPorDiaMySQLNormalizada;
import gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.entidades.UnidadeOperacional;
import gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.repositorios.RepositorioClienteImpl;
import gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.repositorios.RepositorioCompraImpl;
import gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.repositorios.RepositorioProdutoImpl;
import gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.repositorios.RepositorioUnidadeOperacionalImpl;
import gov.sefaz.labs.nosql.compras.mysql.norm.tests.utils.TestUtils;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RepositorioCompraTests {

    @Mock
    private SessionFactory sessionFactory;

    @InjectMocks
    private RepositorioCompraImpl repositorioCompra;

    @InjectMocks
    private RepositorioProdutoImpl repositorioProduto;

    @InjectMocks
    private RepositorioClienteImpl repositorioCliente;

    @InjectMocks
    private RepositorioUnidadeOperacionalImpl repositorioUnidadeOperacional;

    private static SessionFactory sessionFactoryMock;

    @BeforeClass
    public static void classSetUp() throws Exception {
        Configuration config = new Configuration();
        config.addAnnotatedClass(CompraPorDiaMySQLNormalizada.class);
        config.addAnnotatedClass(gov.sefaz.labs.nosql.compras.mysql.norm.persistencia.entidades.Produto.class);
        config.addAnnotatedClass(Cliente.class);
        config.addAnnotatedClass(UnidadeOperacional.class);
        StandardServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(config.getProperties()).build();

        sessionFactoryMock = config.buildSessionFactory(serviceRegistry);
    }

    @AfterClass
    public static void classTearDown() throws Exception {
        sessionFactoryMock.close();
        sessionFactoryMock = null;
    }

    @Before
    public void setUp() throws Exception {
        repositorioCompra.setRepositorioProduto(repositorioProduto);
        repositorioCompra.setRepositorioCliente(repositorioCliente);
        repositorioCompra.setRepositorioUnidadeOperacional(repositorioUnidadeOperacional);
        when(sessionFactory.getCurrentSession()).thenReturn(sessionFactoryMock.getCurrentSession());
        sessionFactory.getCurrentSession().beginTransaction();
    }

    @After
    public void tearDown() throws Exception {
        sessionFactory.getCurrentSession().getTransaction().rollback();
    }

    @Test
    public void testInsira() throws Exception {
        Compra compra = TestUtils.obtenhaInstanciaCompra();
        UUID id = repositorioCompra.insira(compra);

        List<Compra> registros = repositorioCompra.liste();
        assertEquals(1, registros.size());
        assertEquals(id, registros.get(0).getId());
    }

    @Test
    public void testAtualizeComentario() throws Exception {
        Compra compra = TestUtils.obtenhaInstanciaCompra();
        String comentarioOriginal = "comentário original";
        compra.setComentario(comentarioOriginal);

        UUID uuid = repositorioCompra.insira(compra);

        Compra consultado = repositorioCompra.consulte(uuid);
        assertEquals(comentarioOriginal, consultado.getComentario());

        String novoComentario = "novo comentário";
        consultado.setComentario(novoComentario);
        repositorioCompra.atualize(consultado);

        List<Compra> compras = repositorioCompra.liste();
        assertEquals(1, compras.size());

        Compra consultadoAposAtualizacao = repositorioCompra.consulte(uuid);
        assertEquals(novoComentario, consultadoAposAtualizacao.getComentario());
    }

    @Test
    public void testAtualizeProduto() throws Exception {
        Compra compra = TestUtils.obtenhaInstanciaCompra();
        UUID uuid = repositorioCompra.insira(compra);

        String descricaoNovoProduto = "Novo produto";
        EnumCategoriaProduto categoriaNovoProduto = EnumCategoriaProduto.ELETRODOMESTICO;
        double precoNovoProduto = 654.0;
        int codigoNovoProduto = 234234;

        Produto novoProduto = new Produto();
        novoProduto.setDescricao(descricaoNovoProduto);
        novoProduto.setCategoria(categoriaNovoProduto);
        novoProduto.setPrecoUnitario(precoNovoProduto);
        novoProduto.setCodigo(codigoNovoProduto);

        Compra consultado = repositorioCompra.consulte(uuid);
        consultado.setProduto(novoProduto);

        repositorioCompra.atualize(consultado);

        List<Compra> compras = repositorioCompra.liste();
        assertEquals(1, compras.size());

        Compra consultadoAposAtualizacao = repositorioCompra.consulte(uuid);
        Produto produtoAposConsulta = consultadoAposAtualizacao.getProduto();
        assertEquals(descricaoNovoProduto, produtoAposConsulta.getDescricao());
        assertEquals(categoriaNovoProduto, produtoAposConsulta.getCategoria());
        assertEquals(precoNovoProduto, produtoAposConsulta.getPrecoUnitario(), 0.0001);
        assertEquals(codigoNovoProduto, produtoAposConsulta.getCodigo());
    }

    @Test
    public void testExclua() throws Exception {
        Compra compra = TestUtils.obtenhaInstanciaCompra();
        UUID id = repositorioCompra.insira(compra);

        List<Compra> registros = repositorioCompra.liste();
        assertEquals(1, registros.size());

        compra.setId(id);
        repositorioCompra.exclua(compra);
        registros = repositorioCompra.liste();
        assertEquals(0, registros.size());
    }

    @Test
    public void testConsulte() throws Exception {
        Compra compra = TestUtils.obtenhaInstanciaCompra();
        compra.setComentario("comentário específico");
        UUID uuid = repositorioCompra.insira(compra);

        Compra consultado = repositorioCompra.consulte(uuid);
        assertEquals(uuid, consultado.getId());
        assertEquals(compra.getComentario(), consultado.getComentario());
    }

    @Test
    public void testListe() throws Exception {
        repositorioCompra.insira(TestUtils.obtenhaInstanciaCompra());
        repositorioCompra.insira(TestUtils.obtenhaInstanciaCompra());
        repositorioCompra.insira(TestUtils.obtenhaInstanciaCompra());

        List<Compra> listaConsultada = repositorioCompra.liste();
        assertEquals(3, listaConsultada.size());
    }

    @Test
    public void testDadosListe() throws Exception {
        repositorioCompra.insira(TestUtils.obtenhaInstanciaCompra());
        repositorioCompra.insira(TestUtils.obtenhaInstanciaCompra());

        List<Compra> listaConsultada = repositorioCompra.liste();
        Compra compra = listaConsultada.get(0);
        assertNotNull(compra.getId());
        assertNotNull(compra.getDataHora());
        assertNotNull(compra.getCliente());
        assertNotNull(compra.getProduto());
        assertNotNull(compra.getUnidadeOperacional());
    }

    @Test
    public void testListePorData() throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date data1 = dateFormat.parse("20/05/2010");
        Date data2 = dateFormat.parse("15/08/2011");

        Compra compra1 = TestUtils.obtenhaInstanciaCompra();
        compra1.setDataHora(data1);
        Compra compra2 = TestUtils.obtenhaInstanciaCompra();
        compra2.setDataHora(data1);

        Compra compra3 = TestUtils.obtenhaInstanciaCompra();
        compra3.setDataHora(data2);

        repositorioCompra.insira(compra1);
        repositorioCompra.insira(compra2);
        repositorioCompra.insira(compra3);

        List<Compra> comprasData1 = repositorioCompra.listePorData(data1);

        assertEquals(2, comprasData1.size());
    }

    @Test
    public void testExcluaTodos() throws Exception {
        repositorioCompra.insira(TestUtils.obtenhaInstanciaCompra());
        repositorioCompra.insira(TestUtils.obtenhaInstanciaCompra());

        repositorioCompra.excluaTodos();
        List<Compra> listaConsultada = repositorioCompra.liste();
        assertEquals(0, listaConsultada.size());
    }
}
