# Compras - Testes NoSQL
-----------------------------------------

Projeto de testes para comparações de diferentes soluções para a camada de persistência de uma mesma aplicação fictícia (compras) envolvendo cargas relativamente grandes de dados, e dando destaque à comparação de soluções NoSQL entre si e entre estas e uma implementação sob MySQL (relacional).
As diferentes implementações oferecerão operações básicas de leitura e escrita (até então com dados pseudo aleatórios), incluindo especialmente uma consulta de registros de compras para uma data específica.
O objetivo das implementações é permitir com um mínimo de esforço e de maneira relativamente portável a comparação entre os diferentes bancos sob carga de milhões de registros e visando um cenário mais próximo de uma aplicação real, envolvendo múltiplas entidades conceitualmente relacionadas, e tornando evidentes as diferenças de performance advindas da normalização ou denormalização das entidades, validações intrínsecas aos bancos e demais particularidades de cada solução.

[![Build Status](https://drone.io/bitbucket.org/hkmarques/compras/status.png)](https://drone.io/bitbucket.org/hkmarques/compras/latest)

Bancos previstos:

- Cassandra;
- MySQL (estrutura normalizada);
- MySQL (estrutura denormalizada);
- MongoDB;
- Oracle NoSQL.

# Execução das aplicações

- Todas as aplicações usam Gradle como gerenciador de builds e são implementadas usando [Spring Boot](http://projects.spring.io/spring-boot/). Para executar qualquer uma, execute `gradle bootRun` em um terminal (caso o Gradle não esteja instalado, instruções para o download automático seguem abaixo).
- Os projetos de testes de integração (*-tests-spock) associados a cada implementação exigem seus respectivos bancos instalados localmente e com o schema/database `compras` criado.

Obs.: Caso o Gradle não esteja instalado, os scripts do wrapper já inclusos se encarregarão do download e da instalação, basta executar na linha de comando (a partir da raíz do projeto base):

    ./gradlew bootRun       // Unix/Linux
    gradlew.bat bootRun     // Windows