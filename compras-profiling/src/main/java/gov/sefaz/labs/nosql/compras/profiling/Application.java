package gov.sefaz.labs.nosql.compras.profiling;

import gov.sefaz.labs.nosql.compras.core.dominio.Compra;
import gov.sefaz.labs.nosql.compras.core.persistencia.RepositorioCompra;
import gov.sefaz.labs.nosql.compras.core.utils.GeradorDeDados;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

import java.util.List;
import java.util.UUID;

@EnableAutoConfiguration
@ComponentScan(basePackages = {"gov.sefaz.labs.nosql.compras", "gov.sefaz.labs.nosql.compras.profiling.aspectos"})
public class Application implements CommandLineRunner {

    @Autowired
    @SuppressWarnings({"SpringJavaAutowiringInspection", "SpringJavaAutowiredMembersInspection"})
    private RepositorioCompra repositorioCompra;

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(Application.class);
        application.setWebEnvironment(false);
        application.run(args);
    }

    @Override
    public void run(String... args) throws Exception {
        GeradorDeDados geradorDeDados = new GeradorDeDados();
        Compra compra = new Compra();
        geradorDeDados.definaValoresFixos(compra);
        repositorioCompra.excluaTodos();
        List<Compra> compras = repositorioCompra.liste();
        assert 0 == compras.size() : "A base não está limpa!";

        for (int i = 0; i < 30; i++) {
            Compra dadosAleatorios = geradorDeDados.obtenhaCompraComDadosAleatorios();
            compra.setId(dadosAleatorios.getId());    // Não é garantia de definição do ID usado pelo banco.
            compra.setCliente(dadosAleatorios.getCliente());
            compra.setProduto(dadosAleatorios.getProduto());
            compra.setUnidadeOperacional(dadosAleatorios.getUnidadeOperacional());
            compra.setDataHora(dadosAleatorios.getDataHora());

            // Geração dos dados está isolada da medida da duração da operação.
            UUID uuid = repositorioCompra.insira(compra);
            System.out.printf("ID exibido apenas para não haver risco de que a compilação dinâmica elimine a inserção: %s\n", uuid);

            compras = repositorioCompra.liste();
            assert 0 < compras.size() : "O registro não foi inserido!";
            System.out.printf("Total de registros encontrados ao final da operação: %d\n\n", compras.size());
        }

        repositorioCompra.excluaTodos();
    }

}
