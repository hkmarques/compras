package gov.sefaz.labs.nosql.compras.profiling.aspectos;

import gov.sefaz.labs.nosql.compras.core.dominio.Compra;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.UUID;

@Aspect
@Component
public class ProfilingAspect implements Ordered {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_GREEN = "\u001B[32m";
    private static final String nomeArquivo = "teste";
    private static final String caminhoArquivo = "/home/usuario/Desktop/";

    @Pointcut("bean(repositorioCompra)")
    private void metodoRepositorioCompra() {
    }

    @Around("metodoRepositorioCompra() && args(compra)")
    private UUID profile(ProceedingJoinPoint joinPoint, Compra compra) throws Throwable {
        System.out.printf("\nMétodo invocado: %s %s %s\n", ANSI_GREEN, joinPoint.getSignature(), ANSI_RESET);
        StopWatch sw = new StopWatch();
        sw.start();
        UUID uuid = (UUID) joinPoint.proceed(new Object[]{compra});
        sw.stop();
        long duracao = sw.getTotalTimeMillis();
        String conteudo = String.valueOf(duracao) + "\n";

        if (new File(caminhoArquivo).exists()) {
            FileWriter fileWriter = new FileWriter(caminhoArquivo + nomeArquivo, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(conteudo);
            bufferedWriter.flush();
            bufferedWriter.close();
        }

        System.out.printf("Duração: %s %d ms %s\n\n", ANSI_GREEN, duracao, ANSI_RESET);
        return uuid;
    }

    @Override
    public int getOrder() {
        return 1;
    }

}
