package gov.sefaz.labs.nosql.compras.cassandra.tests.unidade.persistencia.entidades;

import gov.sefaz.labs.nosql.compras.cassandra.persistencia.entidades.CompraAchilles;
import gov.sefaz.labs.nosql.compras.core.dominio.Cliente;
import gov.sefaz.labs.nosql.compras.core.dominio.Compra;
import gov.sefaz.labs.nosql.compras.core.dominio.Produto;
import gov.sefaz.labs.nosql.compras.core.dominio.UnidadeOperacional;
import gov.sefaz.labs.nosql.compras.core.utils.TimeUUIDUtils;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class CompraAchillesTest {

    @Test
    public void testConstrutorCompra() throws Exception {
        Compra compra = new Compra();
        Produto produto = new Produto();
        Cliente cliente = new Cliente();
        UnidadeOperacional unidadeOperacional = new UnidadeOperacional();

        UUID idProduto = UUID.randomUUID();
        produto.setId(idProduto);

        UUID idCliente = UUID.randomUUID();
        cliente.setId(idCliente);

        final int codigoUnidadeOperacional = 1234;
        unidadeOperacional.setCodigo(codigoUnidadeOperacional);

        compra.setProduto(produto);
        compra.setCliente(cliente);
        compra.setUnidadeOperacional(unidadeOperacional);
        Calendar data = Calendar.getInstance();
        data.set(2014, Calendar.DECEMBER, 15);
        compra.setDataHora(new Date(data.getTimeInMillis()));

        CompraAchilles compraAchilles = new CompraAchilles(compra);

        assertEquals(compra.getComentario(), compraAchilles.getComentario());
        assertEquals(compra.getQuantidade(), compraAchilles.getQuantidade(), 0.000001);
        assertEquals(compra.getDataHora(), TimeUUIDUtils.getDateFromUUID(compraAchilles.getChave().getData()));

        assertEquals(compra.getProduto(), compraAchilles.getProduto());
        assertEquals(compra.getCliente(), compraAchilles.getCliente());
        assertEquals(compra.getUnidadeOperacional(), compraAchilles.getUnidadeOperacional());
    }

}