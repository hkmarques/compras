package gov.sefaz.labs.nosql.compras.cassandra.tests.utils;

import gov.sefaz.labs.nosql.compras.cassandra.persistencia.entidades.CompraAchilles;
import gov.sefaz.labs.nosql.compras.core.dominio.Compra;
import gov.sefaz.labs.nosql.compras.core.utils.GeradorDeDados;

public class TestUtils {

    public static CompraAchilles obtenhaInstanciaCompraAchilles() {
        return new CompraAchilles(obtenhaInstanciaCompra());
    }

    public static Compra obtenhaInstanciaCompra() {
        return new GeradorDeDados().obtenhaCompraComDadosAleatorios();
    }

}
