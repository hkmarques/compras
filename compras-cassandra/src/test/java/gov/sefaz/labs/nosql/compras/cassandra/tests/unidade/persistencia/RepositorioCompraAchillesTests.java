package gov.sefaz.labs.nosql.compras.cassandra.tests.unidade.persistencia;

import gov.sefaz.labs.nosql.compras.cassandra.persistencia.RepositorioCompraAchilles;
import gov.sefaz.labs.nosql.compras.cassandra.persistencia.entidades.CompraAchilles;
import gov.sefaz.labs.nosql.compras.cassandra.persistencia.entidades.CompraPorDiaAchilles;
import gov.sefaz.labs.nosql.compras.cassandra.tests.utils.TestUtils;
import gov.sefaz.labs.nosql.compras.core.dominio.Compra;
import gov.sefaz.labs.nosql.compras.core.dominio.EnumCategoriaProduto;
import gov.sefaz.labs.nosql.compras.core.dominio.Produto;
import info.archinnov.achilles.junit.AchillesResource;
import info.archinnov.achilles.junit.AchillesResourceBuilder;
import info.archinnov.achilles.persistence.PersistenceManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class RepositorioCompraAchillesTests {

    private RepositorioCompraAchilles repositorio;

    @Rule
    public AchillesResource resource = AchillesResourceBuilder
            .withEntityPackages("gov.sefaz.labs.nosql.compras.cassandra.persistencia.entidades")
            .keyspaceName("compras")
            .tablesToTruncate("compras", "compras_por_dia")
            .truncateBeforeTest()
            .build();

    private PersistenceManager persistenceManager = resource.getPersistenceManager();

    @Before
    public void setUp() throws Exception {
        repositorio = new RepositorioCompraAchilles(persistenceManager, null);
    }

    @Test
    public void testInsira() throws Exception {
        CompraAchilles compra = TestUtils.obtenhaInstanciaCompraAchilles();
        repositorio.insira(compra);

        List<CompraAchilles> registros = persistenceManager.typedQuery(CompraAchilles.class, "select * from compras limit 2").get();
        assertEquals(1, registros.size());
        assertEquals(compra.getChave().getData(), registros.get(0).getChave().getData());
    }

    @Test
    public void testAtualizeComentario() throws Exception {
        CompraAchilles compra = TestUtils.obtenhaInstanciaCompraAchilles();
        String comentarioOriginal = "comentário original";
        compra.setComentario(comentarioOriginal);

        repositorio.insira(compra);

        CompraAchilles consultado = (CompraAchilles) repositorio.consulte(compra.getChave().getData());
        assertEquals(comentarioOriginal, consultado.getComentario());

        String novoComentario = "novo comentário";
        consultado.setComentario(novoComentario);
        repositorio.atualize(consultado);

        List<Compra> compras = repositorio.liste();
        assertEquals(1, compras.size());

        CompraAchilles consultadoAposAtualizacao = (CompraAchilles) repositorio.consulte(compra.getChave().getData());
        assertEquals(novoComentario, consultadoAposAtualizacao.getComentario());
    }

    @Test
    public void testAtualizeProduto() throws Exception {
        CompraAchilles compra = TestUtils.obtenhaInstanciaCompraAchilles();
        repositorio.insira(compra);

        String descricaoNovoProduto = "Novo produto";
        UUID idNovoProduto = UUID.randomUUID();
        EnumCategoriaProduto categoriaNovoProduto = EnumCategoriaProduto.ELETRODOMESTICO;
        double precoNovoProduto = 654.0;
        int codigoNovoProduto = 234234;

        Produto novoProduto = new Produto();
        novoProduto.setDescricao(descricaoNovoProduto);
        novoProduto.setId(idNovoProduto);
        novoProduto.setCategoria(categoriaNovoProduto);
        novoProduto.setPrecoUnitario(precoNovoProduto);
        novoProduto.setCodigo(codigoNovoProduto);

        CompraAchilles consultado = (CompraAchilles) repositorio.consulte(compra.getChave().getData());
        consultado.setProduto(novoProduto);

        repositorio.atualize(consultado);

        List<Compra> compras = repositorio.liste();
        assertEquals(1, compras.size());

        CompraAchilles consultadoAposAtualizacao = (CompraAchilles) repositorio.consulte(compra.getChave().getData());
        Produto produtoAposConsulta = consultadoAposAtualizacao.getProduto();
        assertEquals(descricaoNovoProduto, produtoAposConsulta.getDescricao());
        assertEquals(idNovoProduto, produtoAposConsulta.getId());
        assertEquals(categoriaNovoProduto, produtoAposConsulta.getCategoria());
        assertEquals(precoNovoProduto, produtoAposConsulta.getPrecoUnitario(), 0.0001);
        assertEquals(codigoNovoProduto, produtoAposConsulta.getCodigo());
    }

    @Test
    public void testExclua() throws Exception {
        CompraAchilles compra = TestUtils.obtenhaInstanciaCompraAchilles();
        UUID id = repositorio.insira(compra);

        String query = "select * from compras limit 2";
        List<CompraAchilles> registros = persistenceManager.typedQuery(CompraAchilles.class, query).get();
        assertEquals(1, registros.size());

        compra.setId(id);
        repositorio.exclua(compra);
        registros = persistenceManager.typedQuery(CompraAchilles.class, query).get();

        assertEquals(0, registros.size());
    }

    @Test
    public void testConsulte() throws Exception {
        CompraAchilles compra = TestUtils.obtenhaInstanciaCompraAchilles();
        compra.setComentario("comentário específico");
        persistenceManager.persist(compra);

        CompraAchilles consultado = (CompraAchilles) repositorio.consulte(compra.getChave().getData());
        assertEquals(compra.getChave().getData(), consultado.getChave().getData());
        assertEquals(compra.getComentario(), consultado.getComentario());
    }

    @Test
    public void testListe() throws Exception {
        persistenceManager.persist(TestUtils.obtenhaInstanciaCompraAchilles());
        persistenceManager.persist(TestUtils.obtenhaInstanciaCompraAchilles());
        persistenceManager.persist(TestUtils.obtenhaInstanciaCompraAchilles());

        List<Compra> listaConsultada = repositorio.liste();
        assertEquals(3, listaConsultada.size());
    }

    @Test
    public void testDadosListe() throws Exception {
        persistenceManager.persist(TestUtils.obtenhaInstanciaCompraAchilles());
        persistenceManager.persist(TestUtils.obtenhaInstanciaCompraAchilles());

        List<Compra> listaConsultada = repositorio.liste();
        Compra compra = listaConsultada.get(0);
        assertNotNull(compra.getId());
        assertNotNull(compra.getDataHora());
        assertNotNull(compra.getCliente());
        assertNotNull(compra.getProduto());
        assertNotNull(compra.getUnidadeOperacional());
    }

    @Test
    public void testListePorData() throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date data1 = dateFormat.parse("20/05/2010");
        Date data2 = dateFormat.parse("15/08/2011");

        Compra compra1 = TestUtils.obtenhaInstanciaCompra();
        compra1.setDataHora(data1);
        Compra compra2 = TestUtils.obtenhaInstanciaCompra();
        compra2.setDataHora(data1);

        Compra compra3 = TestUtils.obtenhaInstanciaCompra();
        compra3.setDataHora(data2);

        repositorio.insira(new CompraAchilles(compra1));
        repositorio.insira(new CompraAchilles(compra2));
        repositorio.insira(new CompraAchilles(compra3));

        List<Compra> comprasData1 = repositorio.listePorData(data1);

        assertEquals(2, comprasData1.size());
    }

    @Test
    public void testExcluaTodos() throws Exception {
        repositorio.insira(TestUtils.obtenhaInstanciaCompraAchilles());
        repositorio.insira(TestUtils.obtenhaInstanciaCompraAchilles());

        repositorio.excluaTodos();
        List<CompraAchilles> listaConsultada = persistenceManager.typedQuery(CompraAchilles.class, "select * from compras limit 10").get();
        assertEquals(0, listaConsultada.size());

        List<CompraPorDiaAchilles> comprasPorDia = persistenceManager.typedQuery(CompraPorDiaAchilles.class, "select * from compras_por_dia limit 10").get();
        assertEquals(0, comprasPorDia.size());
    }

    @Test
    public void testInsercaoNasDuasTabelas() throws Exception {
        Compra compra = TestUtils.obtenhaInstanciaCompra();
        repositorio.insira(compra);

        List<CompraAchilles> resultadoTabelaCompras = persistenceManager.typedQuery(CompraAchilles.class, "select * from compras limit 10").get();
        assertEquals(1, resultadoTabelaCompras.size());

        List<CompraPorDiaAchilles> resultadoTabelaComprasPorDia = persistenceManager.typedQuery(CompraPorDiaAchilles.class, "select * from compras_por_dia limit 10").get();
        assertEquals(1, resultadoTabelaComprasPorDia.size());
    }

    @Test
    public void testRemocaoNasDuasTabelas() throws Exception {
        Compra compra = TestUtils.obtenhaInstanciaCompra();
        UUID id = repositorio.insira(compra);

        Compra consultada = repositorio.consulte(id);
        repositorio.exclua(consultada);

        List<CompraAchilles> resultadoTabelaCompras = persistenceManager.typedQuery(CompraAchilles.class, "select * from compras limit 10").get();
        assertEquals(0, resultadoTabelaCompras.size());

        List<CompraPorDiaAchilles> resultadoTabelaComprasPorDia = persistenceManager.typedQuery(CompraPorDiaAchilles.class, "select * from compras_por_dia limit 10").get();
        assertEquals(0, resultadoTabelaComprasPorDia.size());
    }
}