package gov.sefaz.labs.nosql.compras.cassandra.tests.unidade.servicos;

import gov.sefaz.labs.nosql.compras.cassandra.persistencia.RepositorioCompraAchilles;
import gov.sefaz.labs.nosql.compras.cassandra.tests.utils.TestUtils;
import gov.sefaz.labs.nosql.compras.core.dominio.Compra;
import gov.sefaz.labs.nosql.compras.core.servicos.ServicoCompraImpl;
import info.archinnov.achilles.junit.AchillesResource;
import info.archinnov.achilles.junit.AchillesResourceBuilder;
import info.archinnov.achilles.persistence.PersistenceManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ServicoCompraTests {

    private RepositorioCompraAchilles repositorio;

    private ServicoCompraImpl servico;

    @Rule
    public AchillesResource resource = AchillesResourceBuilder
            .withEntityPackages("gov.sefaz.labs.nosql.compras.cassandra.persistencia.entidades")
            .keyspaceName("compras")
            .tablesToTruncate("compras", "compras_por_dia")
            .truncateBeforeTest()
            .build();

    private PersistenceManager persistenceManager = resource.getPersistenceManager();

    @Before
    public void setUp() throws Exception {
        repositorio = new RepositorioCompraAchilles(persistenceManager, null);
        servico = new ServicoCompraImpl(repositorio);
    }

    @Test
    public void testInsercao() throws Exception {
        Compra compra = TestUtils.obtenhaInstanciaCompra();
        servico.insira(compra);

        assertEquals(1, repositorio.liste().size());
    }

    @Test
    public void testConsulta() throws Exception {
        Compra compra = TestUtils.obtenhaInstanciaCompra();
        servico.insira(compra);

        Compra resultadoConsulta = servico.consulte(compra.getId());
        assertNotNull(resultadoConsulta);
    }

    @Test
    public void testExclusao() throws Exception {
        Compra compra = TestUtils.obtenhaInstanciaCompra();
        servico.insira(compra);

        servico.exclua(compra);
        assertEquals(0, repositorio.liste().size());
    }

    @Test
    public void testAtualizacaoComentario() throws Exception {
        Compra compra = TestUtils.obtenhaInstanciaCompra();
        compra.setComentario("comentário original");
        UUID id = servico.insira(compra);

        String novoComentario = "novo comentário";
        compra.setComentario(novoComentario);
        compra.setId(id);
        servico.atualize(compra);

        Compra resultadoConsulta = repositorio.consulte(id);
        assertEquals(novoComentario, resultadoConsulta.getComentario());
    }

    @Test
    public void testListe() throws Exception {
        Compra compra1 = TestUtils.obtenhaInstanciaCompra();
        Compra compra2 = TestUtils.obtenhaInstanciaCompra();

        servico.insira(compra1);
        servico.insira(compra2);

        List<Compra> compras = servico.liste();
        assertEquals(2, compras.size());
    }

    @Test
    public void testListePorData() throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date data1 = dateFormat.parse("20/05/2010");
        Date data2 = dateFormat.parse("15/08/2011");

        Compra compra1 = TestUtils.obtenhaInstanciaCompra();
        compra1.setDataHora(data1);
        Compra compra2 = TestUtils.obtenhaInstanciaCompra();
        compra2.setDataHora(data1);

        Compra compra3 = TestUtils.obtenhaInstanciaCompra();
        compra3.setDataHora(data2);

        servico.insira(compra1);
        servico.insira(compra2);
        servico.insira(compra3);

        List<Compra> compras = servico.listePorData(data1);
        assertEquals(2, compras.size());
    }
}
