package gov.sefaz.labs.nosql.compras.cassandra.tests.unidade.persistencia;

import gov.sefaz.labs.nosql.compras.cassandra.persistencia.entidades.CompraAchilles;
import gov.sefaz.labs.nosql.compras.cassandra.tests.utils.TestUtils;
import info.archinnov.achilles.junit.AchillesResource;
import info.archinnov.achilles.junit.AchillesResourceBuilder;
import info.archinnov.achilles.persistence.PersistenceManager;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class PersistenceManagerTests {

    @Rule
    public AchillesResource resource = AchillesResourceBuilder
            .withEntityPackages("gov.sefaz.labs.nosql.compras.cassandra.persistencia.entidades")
            .keyspaceName("compras")
            .tablesToTruncate("compras", "compras_por_dia")
            .build();

    private PersistenceManager persistenceManager = resource.getPersistenceManager();

    @Test
    public void testPersist() throws Exception {
        CompraAchilles compra = TestUtils.obtenhaInstanciaCompraAchilles();
        assertEquals(0, consulteComprasPorDia().size());

        persistenceManager.persist(compra);

        List<CompraAchilles> registros = consulteComprasPorDia();
        assertEquals(1, registros.size());
    }

    @Test
    public void testRemove() throws Exception {
        CompraAchilles compra = TestUtils.obtenhaInstanciaCompraAchilles();
        persistenceManager.persist(compra);
        persistenceManager.remove(compra);

        List<CompraAchilles> registros = consulteComprasPorDia();
        assertEquals(0, registros.size());
    }

    @Test
    public void testRemoveById() throws Exception {
        CompraAchilles compra = TestUtils.obtenhaInstanciaCompraAchilles();
        persistenceManager.persist(compra);
        persistenceManager.persist(TestUtils.obtenhaInstanciaCompraAchilles());
        persistenceManager.removeById(CompraAchilles.class, compra.getChave());

        List<CompraAchilles> registros = consulteComprasPorDia();
        assertEquals(1, registros.size());
    }

    @Test
    public void testUpdate() throws Exception {
        CompraAchilles compra = TestUtils.obtenhaInstanciaCompraAchilles();
        compra.setQuantidade(12343.23);
        persistenceManager.persist(compra);

        CompraAchilles.ChaveCompraAchilles chave = compra.getChave();
        CompraAchilles novosDados = persistenceManager.find(CompraAchilles.class, chave);
        double quantidadeNova = 45234.34;
        novosDados.setQuantidade(quantidadeNova);

        persistenceManager.update(novosDados);
        CompraAchilles resultadoDaConsulta = persistenceManager.find(CompraAchilles.class, chave);
        assertEquals(quantidadeNova, resultadoDaConsulta.getQuantidade(), 0.000001);
    }

    private List<CompraAchilles> consulteComprasPorDia() {
        return persistenceManager.typedQuery(CompraAchilles.class, "select * from compras limit 2").get();
    }
}
