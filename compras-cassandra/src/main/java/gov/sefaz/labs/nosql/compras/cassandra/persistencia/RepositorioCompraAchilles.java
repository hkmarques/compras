package gov.sefaz.labs.nosql.compras.cassandra.persistencia;

import gov.sefaz.labs.nosql.compras.cassandra.persistencia.entidades.CompraAchilles;
import gov.sefaz.labs.nosql.compras.cassandra.persistencia.entidades.CompraAchillesAbstrata;
import gov.sefaz.labs.nosql.compras.cassandra.persistencia.entidades.CompraPorDiaAchilles;
import gov.sefaz.labs.nosql.compras.core.dominio.Cliente;
import gov.sefaz.labs.nosql.compras.core.dominio.Compra;
import gov.sefaz.labs.nosql.compras.core.dominio.Produto;
import gov.sefaz.labs.nosql.compras.core.dominio.UnidadeOperacional;
import gov.sefaz.labs.nosql.compras.core.persistencia.RepositorioCompra;
import gov.sefaz.labs.nosql.compras.core.utils.GeradorDeDados;
import gov.sefaz.labs.nosql.compras.core.utils.TimeUUIDUtils;
import info.archinnov.achilles.persistence.BatchingPersistenceManager;
import info.archinnov.achilles.persistence.PersistenceManager;
import info.archinnov.achilles.persistence.PersistenceManagerFactory;
import info.archinnov.achilles.query.typed.TypedQuery;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.management.ManagementFactory;
import java.util.*;

@Repository("repositorioCompra")
@Scope("prototype")
public class RepositorioCompraAchilles implements RepositorioCompra {

    private final String nomeCFCompras = "compras";

    private final String nomeCFComprasPorDia = "compras_por_dia";

    private final String queryCompraPorDia = "select * from " + nomeCFComprasPorDia + " where ano = ? and mes = ? and dia = ? and data = ?";

    private final String queryCompras = "select * from " + nomeCFCompras + " where data = ?";

    private PersistenceManager persistenceManager;

    @Autowired
    private PersistenceManagerFactory persistenceManagerFactory;

    public RepositorioCompraAchilles() {
    }

    public RepositorioCompraAchilles(
            PersistenceManager persistenceManager,
            PersistenceManagerFactory persistenceManagerFactory) {
        this.persistenceManager = persistenceManager;
        this.persistenceManagerFactory = persistenceManagerFactory;
    }

    @PostConstruct
    public void init() {
        persistenceManager = persistenceManagerFactory.createPersistenceManager();
    }

    @PreDestroy
    public void destroy() {
        persistenceManager.getNativeSession().close();
    }

    @Override
    public Long insiraEmLote(long quantidade, boolean valoresFixos) {
        GeradorDeDados geradorDeDados = new GeradorDeDados();

        long tempoEscrita;

        BatchingPersistenceManager batchingPersistenceManager = persistenceManagerFactory.createBatchingPersistenceManager();
        batchingPersistenceManager.startBatch();    // começa a armazenar operações.

        UUID idFixo = UUID.fromString("c08dea11-631d-45cf-bc26-b2e2f6daadd4");
        for (int i = 0; i < quantidade; i++) {
            CompraAchilles compraPorDia = new CompraAchilles();
            CompraAchilles.ChaveCompraAchilles chave = compraPorDia.getChave();

            if (valoresFixos) {
                geradorDeDados.definaValoresFixos(compraPorDia);
                compraPorDia.getCliente().setId(idFixo);
                compraPorDia.getProduto().setId(idFixo);
            } else {
                geradorDeDados.definaValoresAleatorios(compraPorDia);
            }

            chave.setIdCliente(compraPorDia.getProduto().getId());
            chave.setIdProduto(compraPorDia.getCliente().getId());
            chave.setData(TimeUUIDUtils.convertaDataParaUUID(compraPorDia.getDataHora()));

            batchingPersistenceManager.persist(compraPorDia);   // Apenas adiciona a operação ao batch.
        }

        tempoEscrita = System.nanoTime();
        long pid = Long.valueOf(ManagementFactory.getRuntimeMXBean().getName().split("@")[0]);
        long ppid = Thread.currentThread().getId();
        try {
            batchingPersistenceManager.endBatch();              // Momento do flush.
            tempoEscrita = System.nanoTime() - tempoEscrita;
            System.out.printf("Processo %d    Thread %d:     +%d registro(s) inserido(s) em %.3f s.\n", pid, ppid, quantidade, tempoEscrita / 1000000000.0);
        } catch (Exception e) {
            System.out.printf("Processo %d    Thread %d:     Nó não confirmou inserção de %d registros!\n", pid, ppid, quantidade);
            String nomeArquivoErro = "/home/usuario/DEV/" + pid + "-" + ppid;
            try {
                e.printStackTrace(new PrintStream(new File(nomeArquivoErro)));
            } catch (IOException efile) {
                System.out.println("Erro ao tentar escrever arquivo " + nomeArquivoErro);
            }

            tempoEscrita = System.nanoTime() - tempoEscrita;
        }

        return tempoEscrita;
    }

    @Override
    public UUID insira(Compra compra) {
        CompraAchilles compraAchilles = new CompraAchilles(compra);

        // Obs.: Para o projeto de testes o persistence manager padrão está sendo
        // usado, porém, em ambiente de produção, a versão para processamento de batch seria
        // melhor pelo comportamento transacional.
        persistenceManager.persist(compraAchilles);

        CompraPorDiaAchilles compraPorDiaAchilles = new CompraPorDiaAchilles(compra);
        UUID dataId = compraAchilles.getChave().getData();
        compraPorDiaAchilles.getChave().setData(dataId);
        persistenceManager.persist(compraPorDiaAchilles);

        return dataId;
    }

    @Override
    public void atualize(Compra compra) {
        CompraAchilles novosValores = new CompraAchilles(compra);
        UUID dataChave = novosValores.getChave().getData();
        CompraAchilles instanciaConsultada = (CompraAchilles) this.consulte(dataChave);
        if (haAlteracaoEmCamposChaves(instanciaConsultada, novosValores)) {
            this.exclua(instanciaConsultada);
            this.insira(novosValores);
            return;
        }

        atualizeInstanciaConsultada(novosValores, instanciaConsultada);
        persistenceManager.update(instanciaConsultada);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(compra.getDataHora());
        List<CompraPorDiaAchilles> comprasPorDia = persistenceManager
                .typedQuery(CompraPorDiaAchilles.class, queryCompraPorDia, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), dataChave)
                .get();

        assert 0 < comprasPorDia.size() : String.format("A entrada correspondente à compra de id %s na CF " + nomeCFComprasPorDia + " não foi encontrada", dataChave);
        CompraPorDiaAchilles compraPorDiaAchilles = comprasPorDia
                .get(0);

        atualizeInstanciaConsultada(new CompraPorDiaAchilles(compra), compraPorDiaAchilles);
        persistenceManager.update(compraPorDiaAchilles);
    }

    @Override
    public void exclua(Compra compra) {
        UUID dataChave = compra.getId();
        CompraAchilles compraAchilles = (CompraAchilles) this.consulte(dataChave);

        assert compraAchilles != null : "O registro não foi encontrado na CF compras";
        persistenceManager.remove(compraAchilles);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(TimeUUIDUtils.getDateFromUUID(dataChave));
        List<CompraPorDiaAchilles> comprasPorDia = persistenceManager
                .typedQuery(CompraPorDiaAchilles.class, queryCompraPorDia, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), dataChave)
                .get();

        assert 0 < comprasPorDia.size() : String.format("A entrada correspondente à compra de id %s na CF " + nomeCFComprasPorDia + " não foi encontrada", dataChave);
        CompraPorDiaAchilles compraPorDiaAchilles = comprasPorDia
                .get(0);

        persistenceManager.remove(compraPorDiaAchilles);
    }

    @Override
    public Compra consulte(UUID id) {
        TypedQuery<CompraAchilles> query = persistenceManager.typedQuery(CompraAchilles.class, queryCompras, id);
        List<CompraAchilles> compras = query.get();
        if (compras.isEmpty()) {
            return null;
        }

        CompraAchilles compra = compras.get(0);


        return convertaCompraConsultada(compra);
    }

    @Override
    public List<Compra> liste() {
        List<CompraAchilles> resultadoConsulta = persistenceManager.typedQuery(CompraAchilles.class, "select * from " + nomeCFCompras).get();
        List<Compra> resultadoConvertido = new ArrayList<Compra>();
        for (CompraAchilles compra : resultadoConsulta) {
            resultadoConvertido.add(convertaCompraConsultada(compra));
        }

        return resultadoConvertido;
    }

    @Override
    public List<Compra> listePorData(Date data) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(data);
        List<CompraPorDiaAchilles> resultadoConsulta = persistenceManager.typedQuery(CompraPorDiaAchilles.class, "select * from " + nomeCFComprasPorDia + " where ano = ? and mes = ? and dia = ?", calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE)).get();
        List<Compra> resultadoConvertido = new ArrayList<Compra>();
        for (CompraPorDiaAchilles compra : resultadoConsulta) {
            resultadoConvertido.add(convertaCompraConsultada(compra));
        }

        return resultadoConvertido;
    }

    @Override
    public void excluaTodos() {
        persistenceManager.getNativeSession().execute("truncate compras." + nomeCFCompras);
        persistenceManager.getNativeSession().execute("truncate compras." + nomeCFComprasPorDia);
    }

    private boolean haAlteracaoEmCamposChaves(CompraAchilles valoresOriginais, CompraAchilles novosValores) {
        CompraAchilles.ChaveCompraAchilles chaveOriginal = valoresOriginais.getChave();
        CompraAchilles.ChaveCompraAchilles chaveNova = novosValores.getChave();
        return ! (chaveOriginal.getIdCliente().equals(chaveNova.getIdCliente())
                && chaveOriginal.getIdProduto().equals(chaveNova.getIdProduto()));
    }

    private void atualizeInstanciaConsultada(Compra novosDados, Compra instanciaConsultada) {
        BeanUtils.copyProperties(novosDados, instanciaConsultada, "chave", "cliente", "produto", "unidadeOperacional", "dataHora");
        Cliente cliente = novosDados.getCliente();
        instanciaConsultada.setCliente(cliente);
        instanciaConsultada.setProduto(novosDados.getProduto());
        instanciaConsultada.setUnidadeOperacional(novosDados.getUnidadeOperacional());
    }

    private Compra convertaCompraConsultada(CompraAchillesAbstrata compra) {
        Cliente cliente = new Cliente();
        Produto produto = new Produto();
        UnidadeOperacional unidadeOperacional = new UnidadeOperacional();

        UUID dataTimeuuid;
        if (CompraAchilles.class.isInstance(compra)) {
            CompraAchilles compraAchilles = (CompraAchilles) compra;
            cliente.setId(compraAchilles.getChave().getIdCliente());
            produto.setId(compraAchilles.getChave().getIdProduto());
            dataTimeuuid = compraAchilles.getChave().getData();
        } else {
            CompraPorDiaAchilles compraPorDiaAchilles = (CompraPorDiaAchilles) compra;
            cliente.setId(compraPorDiaAchilles.getChave().getIdCliente());
            produto.setId(compraPorDiaAchilles.getChave().getIdProduto());
            dataTimeuuid = compraPorDiaAchilles.getChave().getData();
        }

        cliente.setNome(compra.getNomeCliente());
        compra.setCliente(cliente);

        produto.setPrecoUnitario(compra.getPrecoUnitarioProduto());
        produto.setCodigo(compra.getCodigoProduto());
        produto.setCategoria(compra.getCategoriaProduto());
        produto.setDescricao(compra.getDescricaoProduto());
        compra.setProduto(produto);

        unidadeOperacional.setCodigo(compra.getCodigoUnidadeOperacional());
        unidadeOperacional.setDescricao(compra.getDescricaoUnidadeOperacional());
        compra.setUnidadeOperacional(unidadeOperacional);

        compra.setId(dataTimeuuid);
        compra.setDataHora(TimeUUIDUtils.getDateFromUUID(dataTimeuuid));

        return compra;
    }
}
