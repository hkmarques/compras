package gov.sefaz.labs.nosql.compras.cassandra.persistencia.entidades;

import gov.sefaz.labs.nosql.compras.core.dominio.Cliente;
import gov.sefaz.labs.nosql.compras.core.dominio.Compra;
import gov.sefaz.labs.nosql.compras.core.dominio.Produto;
import gov.sefaz.labs.nosql.compras.core.utils.TimeUUIDUtils;
import info.archinnov.achilles.annotations.*;
import org.springframework.beans.BeanUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Entity(table = "compras_por_dia")
public class CompraPorDiaAchilles extends CompraAchillesAbstrata {

    @EmbeddedId
    private ChaveComprasPorDiaAchilles chave;

    public CompraPorDiaAchilles() {
        super();
        this.chave = new ChaveComprasPorDiaAchilles();
    }

    public CompraPorDiaAchilles(Date data, UUID idCliente, UUID idProduto) {
        this();
        this.chave = new ChaveComprasPorDiaAchilles(data, idCliente, idProduto);
        this.cliente.setId(idCliente);
        this.produto.setId(idProduto);
    }

    public CompraPorDiaAchilles(Compra compra) {
        this();
        BeanUtils.copyProperties(compra, this, "cliente", "produto", "unidadeOperacional", "dataHora");
        this.setProduto(compra.getProduto());
        this.setCliente(compra.getCliente());
        this.setUnidadeOperacional(compra.getUnidadeOperacional());
        Date dataHora = compra.getDataHora();
        this.setDataHora(dataHora);
        UUID data = CompraPorDiaAchilles.class.isInstance(compra)
                    ? ((CompraPorDiaAchilles)compra).getChave().getData()
                    : TimeUUIDUtils.convertaDataParaUUID(dataHora);

        this.chave.setData(data);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dataHora);
        this.chave.setAno(calendar.get(Calendar.YEAR));
        this.chave.setMes(calendar.get(Calendar.MONTH));
        this.chave.setDia(calendar.get(Calendar.DATE));
    }

    /**
     * Classe representativa da chave (composta) da column family compras.
     */
    public static class ChaveComprasPorDiaAchilles {

        @Order(1)
        @Column(name = "ano")
        @PartitionKey
        private int ano;

        @Order(2)
        @Column(name = "mes")
        @PartitionKey
        private int mes;

        @Order(3)
        @Column(name = "dia")
        @PartitionKey
        private int dia;

        @Order(4)
        @Column(name = "data")
        @TimeUUID
        private UUID data;

        @Order(5)
        @Column(name = "cliente_id")
        private UUID idCliente;

        @Order(6)
        @Column(name = "produto_id")
        private UUID idProduto;

        public ChaveComprasPorDiaAchilles() {
        }

        public ChaveComprasPorDiaAchilles(Date data, UUID idCliente, UUID idProduto) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(data);
            this.ano = calendar.get(Calendar.YEAR);
            this.mes = calendar.get(Calendar.MONTH);
            this.dia = calendar.get(Calendar.DATE);
            this.data = TimeUUIDUtils.convertaDataParaUUID(data);

            this.idCliente = idCliente;
            this.idProduto = idProduto;
        }

        public int getAno() {
            return ano;
        }

        public void setAno(int ano) {
            this.ano = ano;
        }

        public int getMes() {
            return mes;
        }

        public void setMes(int mes) {
            this.mes = mes;
        }

        public int getDia() {
            return dia;
        }

        public void setDia(int dia) {
            this.dia = dia;
        }

        public UUID getIdCliente() {
            return idCliente;
        }

        public void setIdCliente(UUID idCliente) {
            this.idCliente = idCliente;
        }

        public UUID getIdProduto() {
            return idProduto;
        }

        public void setIdProduto(UUID idProduto) {
            this.idProduto = idProduto;
        }

        public UUID getData() {
            return data;
        }

        public void setData(UUID data) {
            this.data = data;
        }
    }

    // ------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------
    // ----------------  SETTERS/GETTERS SOBRESCRITOS DE COMPRAS (CORE) -------------
    // ------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------

    @Override
    public void setCliente(Cliente cliente) {
        super.setCliente(cliente);

        // Define também os valores do ID e do nome do cliente na estrutura denormalizada quando
        // a instância de cliente associada é redefinida.
        this.chave.setIdCliente(cliente.getId());
        this.setNomeCliente(cliente.getNome());
    }

    @Override
    public void setProduto(Produto produto) {
        super.setProduto(produto);

        // Define também os valores associados ao produto para a estrutura denormalizada (Cassandra)
        // quando a instância de produto associada é redefinida.
        this.chave.setIdProduto(produto.getId());
        this.setCodigoProduto(produto.getCodigo());
        this.setDescricaoProduto(produto.getDescricao());
        this.setCategoriaProduto(produto.getCategoria());
        this.setPrecoUnitarioProduto(produto.getPrecoUnitario());
    }

    public ChaveComprasPorDiaAchilles getChave() {
        return chave;
    }

    public void setChave(ChaveComprasPorDiaAchilles chave) {
        this.chave = chave;
    }

}
