package gov.sefaz.labs.nosql.compras.cassandra.persistencia;

import com.datastax.driver.core.ProtocolOptions;
import info.archinnov.achilles.persistence.PersistenceManagerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfiguracaoAchilles {

    @Bean
    public PersistenceManagerFactory persistenceManagerFactory() {
        return PersistenceManagerFactory.PersistenceManagerFactoryBuilder
                .builder()
                .withEntityPackages("gov.sefaz.labs.nosql.compras.cassandra.persistencia.entidades")
                .withConnectionContactPoints("localhost")
                .withCQLPort(9042)  // porta usando protocolo binário
                .withKeyspaceName("compras")
                .forceTableCreation(true)
                .withCompressionType(ProtocolOptions.Compression.NONE)
                .build();
    }
}
