package gov.sefaz.labs.nosql.compras.cassandra.persistencia.entidades;

import gov.sefaz.labs.nosql.compras.core.dominio.Cliente;
import gov.sefaz.labs.nosql.compras.core.dominio.Compra;
import gov.sefaz.labs.nosql.compras.core.dominio.Produto;
import gov.sefaz.labs.nosql.compras.core.utils.TimeUUIDUtils;
import info.archinnov.achilles.annotations.*;
import org.springframework.beans.BeanUtils;

import java.util.Date;
import java.util.UUID;

@Entity(table = "compras")
public class CompraAchilles extends CompraAchillesAbstrata {

    @EmbeddedId
    private ChaveCompraAchilles chave;

    public CompraAchilles() {
        super();
        this.chave = new ChaveCompraAchilles();
    }

    public CompraAchilles(Date data, UUID idCliente, UUID idProduto) {
        this();
        UUID dataUUID = TimeUUIDUtils.convertaDataParaUUID(data);
        this.chave = new ChaveCompraAchilles(dataUUID, idCliente, idProduto);
        this.cliente.setId(idCliente);
        this.produto.setId(idProduto);
    }

    public CompraAchilles(Compra compra) {
        this();
        BeanUtils.copyProperties(compra, this, "cliente", "produto", "unidadeOperacional", "dataHora");
        this.setProduto(compra.getProduto());
        this.setCliente(compra.getCliente());
        this.setUnidadeOperacional(compra.getUnidadeOperacional());
        Date dataHora = compra.getDataHora();
        this.setDataHora(dataHora);

        UUID data = CompraAchilles.class.isInstance(compra)
            ? ((CompraAchilles)compra).getChave().getData()
            : TimeUUIDUtils.convertaDataParaUUID(dataHora);

        this.chave.setData(data);
    }

    /**
     * Classe representativa da chave (composta) da column family compras.
     */
    public static class ChaveCompraAchilles {

        @Order(1)
        @TimeUUID
        @Column(name = "data")
        private UUID data;

        @Order(2)
        @Column(name = "cliente_id")
        private UUID idCliente;

        @Order(3)
        @Column(name = "produto_id")
        private UUID idProduto;

        public ChaveCompraAchilles() {
        }

        public ChaveCompraAchilles(UUID data, UUID idCliente, UUID idProduto) {
            this.data = data;
            this.idCliente = idCliente;
            this.idProduto = idProduto;
        }

        public UUID getData() {
            return data;
        }

        public void setData(UUID data) {
            this.data = data;
        }

        public UUID getIdCliente() {
            return idCliente;
        }

        public void setIdCliente(UUID idCliente) {
            this.idCliente = idCliente;
        }

        public UUID getIdProduto() {
            return idProduto;
        }

        public void setIdProduto(UUID idProduto) {
            this.idProduto = idProduto;
        }
    }

    // ------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------
    // ----------------  SETTERS/GETTERS SOBRESCRITOS DE COMPRAS (CORE) -------------
    // ------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------

    @Override
    public void setCliente(Cliente cliente) {
        super.setCliente(cliente);

        // Define também os valores do ID e do nome do cliente na estrutura denormalizada quando
        // a instância de cliente associada é redefinida.
        this.chave.setIdCliente(cliente.getId());
        this.setNomeCliente(cliente.getNome());
    }

    @Override
    public void setProduto(Produto produto) {
        super.setProduto(produto);

        // Define também os valores associados ao produto para a estrutura denormalizada (Cassandra)
        // quando a instância de produto associada é redefinida.
        this.chave.setIdProduto(produto.getId());
        this.setCodigoProduto(produto.getCodigo());
        this.setDescricaoProduto(produto.getDescricao());
        this.setCategoriaProduto(produto.getCategoria());
        this.setPrecoUnitarioProduto(produto.getPrecoUnitario());
    }

    // ------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------
    // ------------- FIM: SETTERS/GETTERS SOBRESCRITOS DE COMPRAS (CORE) ------------
    // ------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------

    public ChaveCompraAchilles getChave() {
        return chave;
    }

    public void setChave(ChaveCompraAchilles chave) {
        this.chave = chave;
    }

}

