package gov.sefaz.labs.nosql.compras.cassandra.persistencia.entidades;

import gov.sefaz.labs.nosql.compras.core.dominio.*;
import info.archinnov.achilles.annotations.Column;

public abstract class CompraAchillesAbstrata extends Compra {

    public CompraAchillesAbstrata() {
        this.cliente = new Cliente();
        this.produto = new Produto();
        this.unidadeOperacional = new UnidadeOperacional();
    }

    @Column(name = "comentario")
    private String comentario;

    @Column(name = "quantidade")
    private double quantidade;

    @Column(name = "cliente_nome")
    private String nomeCliente;

    @Column(name = "unid_operacional_cod")
    private long codigoUnidadeOperacional;

    @Column(name = "unid_operacional_desc")
    private String descricaoUnidadeOperacional;

    @Column(name = "produto_desc")
    private String descricaoProduto;

    @Column(name = "produto_categoria")
    private EnumCategoriaProduto categoriaProduto;

    @Column(name = "produto_cod")
    private long codigoProduto;

    @Column(name = "produto_preco_unitario")
    private double precoUnitarioProduto;

    @Override
    public void setUnidadeOperacional(UnidadeOperacional unidadeOperacional) {
        super.setUnidadeOperacional(unidadeOperacional);

        // Define também os valores associados à unidade operacional para a estrutura denormalizada
        // (Cassandra) quando a instância de unidade operacional é redefinida.
        this.setCodigoUnidadeOperacional(unidadeOperacional.getCodigo());
        this.setDescricaoUnidadeOperacional(unidadeOperacional.getDescricao());
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public long getCodigoUnidadeOperacional() {
        return codigoUnidadeOperacional;
    }

    public void setCodigoUnidadeOperacional(long codigoUnidadeOperacional) {
        this.codigoUnidadeOperacional = codigoUnidadeOperacional;
    }

    public String getDescricaoUnidadeOperacional() {
        return descricaoUnidadeOperacional;
    }

    public void setDescricaoUnidadeOperacional(String descricaoUnidadeOperacional) {
        this.descricaoUnidadeOperacional = descricaoUnidadeOperacional;
    }

    public String getDescricaoProduto() {
        return descricaoProduto;
    }

    public void setDescricaoProduto(String descricaoProduto) {
        this.descricaoProduto = descricaoProduto;
    }

    public EnumCategoriaProduto getCategoriaProduto() {
        return categoriaProduto;
    }

    public void setCategoriaProduto(EnumCategoriaProduto categoriaProduto) {
        this.categoriaProduto = categoriaProduto;
    }

    public long getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(long codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    public double getPrecoUnitarioProduto() {
        return precoUnitarioProduto;
    }

    public void setPrecoUnitarioProduto(double precoUnitarioProduto) {
        this.precoUnitarioProduto = precoUnitarioProduto;
    }

    @Override
    public String getComentario() {
        return this.comentario;
    }

    @Override
    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    @Override
    public double getQuantidade() {
        return this.quantidade;
    }

    @Override
    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }
}
